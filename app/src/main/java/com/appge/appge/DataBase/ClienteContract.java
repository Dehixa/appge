package com.appge.appge.DataBase;

import android.provider.BaseColumns;

public class ClienteContract {
    public static abstract class ClienteEntry implements BaseColumns {
        public static final String TABLE_NAME = "cliente";

        public static final String NOMBRES = "cli_nombres";
        public static final String TELEFONO = "cli_telefono";
        public static final String EMAIL = "cli_email";
        public static final String DIRECCION = "cli_direccion";
        public static final String INFORMACION = "cli_info";
    }
}
