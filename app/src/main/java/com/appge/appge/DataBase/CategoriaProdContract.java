package com.appge.appge.DataBase;

import android.provider.BaseColumns;

public class CategoriaProdContract {

    public static abstract class CategoriaProdEntry implements BaseColumns {

        public static final String TABLE_NAME = "categoria_prod";

        public static final String NOMBRE = "cat_prod_nombre";

    }

}
