package com.appge.appge.DataBase;

import android.provider.BaseColumns;

public class ProductoContract {

    public static abstract class ProductoEntry implements BaseColumns {

        public static final String TABLE_NAME = "producto";

        public static final String NOMBRE = "prod_nombre";
        public static final String UNIDAD_COD = "unidad_cod";
        public static final String CATEGORIA_COD = "categoria_cod";
        public static final String CANTIDAD = "prod_cantidad";
        public static final String CANTIDAD_RESERVA = "prod_cantidad_reserva";
        public static final String PRECIO_COMPRA = "prod_precio_compra";
        public static final String PRECIO_VENTA = "prod_precio_venta";
        public static final String FECHA = "prod_fecha";
        public static final String INFORMACION = "prod_info";
        public static final String IMAGEN = "prod_imagen";

    }

}
