package com.appge.appge.DataBase;

import android.provider.BaseColumns;

public class VentaDetContract {
    public static abstract class VentaDetEntry implements BaseColumns {
        public static final String TABLE_NAME = "venta_detalle";

        public static final String VENTA_CAB_ID = "venta_det_venta_id";
        public static final String PRODUCTO_ID = "venta_det_producto_id";
        public static final String CANTIDAD = "venta_det_cantidad";
        public static final String SUBTOTAL = "venta_det_subtotal";
    }
}
