package com.appge.appge.DataBase;

import android.provider.BaseColumns;

public class PersonalContract {

    public static abstract class PersonalEntry implements BaseColumns {
        public static final String TABLE_NAME = "personal";

        public static final String NOMBRES = "per_nombres";
        public static final String PUESTO = "per_puesto";
        public static final String SALARIO = "per_salario";
        public static final String TELEFONO = "per_telefono";
        public static final String EMAIL = "per_email";
        public static final String INFORMACION = "per_info";
    }

}
