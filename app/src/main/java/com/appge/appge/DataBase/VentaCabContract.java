package com.appge.appge.DataBase;

import android.provider.BaseColumns;

public class VentaCabContract {
    public static abstract class VentaCabEntry implements BaseColumns {
        public static final String TABLE_NAME = "venta_cabecera";

        public static final String FECHA = "venta_cab_fecha";
        public static final String CLIENTE = "venta_cab_cliente";
        public static final String FORMA_PAGO = "venta_cab_forma_pago";
        public static final String TOTAL = "venta_cab_total";
    }
}
