package com.appge.appge.DataBase;

import android.provider.BaseColumns;

public class UnidadContract {

    public static abstract class UnidadEntry implements BaseColumns {

        public static final String TABLE_NAME = "unidad";

        public static final String NOMBRE = "uni_nombre";

    }

}
