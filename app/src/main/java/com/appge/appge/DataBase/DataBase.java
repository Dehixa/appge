package com.appge.appge.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "negocio.db";

    public DataBase (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + ProductoContract.ProductoEntry.TABLE_NAME + " ("
                                + ProductoContract.ProductoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                                + ProductoContract.ProductoEntry.NOMBRE + " TEXT NOT NULL,"
                                + ProductoContract.ProductoEntry.UNIDAD_COD + " INTEGER NOT NULL,"
                                + ProductoContract.ProductoEntry.CATEGORIA_COD + " INTEGER NOT NULL,"
                                + ProductoContract.ProductoEntry.CANTIDAD + " INTEGER NOT NULL,"
                                + ProductoContract.ProductoEntry.CANTIDAD_RESERVA + " INTEGER NOT NULL,"
                                + ProductoContract.ProductoEntry.PRECIO_COMPRA + " REAL NOT NULL,"
                                + ProductoContract.ProductoEntry.PRECIO_VENTA + " REAL NOT NULL,"
                                + ProductoContract.ProductoEntry.FECHA + " TEXT,"
                                + ProductoContract.ProductoEntry.INFORMACION + " TEXT,"
                                + ProductoContract.ProductoEntry.IMAGEN + " BLOB);");

        sqLiteDatabase.execSQL("CREATE TABLE " + UnidadContract.UnidadEntry.TABLE_NAME + " ("
                                + UnidadContract.UnidadEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                                + UnidadContract.UnidadEntry.NOMBRE + " TEXT NOT NULL);");

        sqLiteDatabase.execSQL("CREATE TABLE " + CategoriaProdContract.CategoriaProdEntry.TABLE_NAME + " ("
                                + CategoriaProdContract.CategoriaProdEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                                + CategoriaProdContract.CategoriaProdEntry.NOMBRE + " TEXT NOT NULL);");

        sqLiteDatabase.execSQL("CREATE TABLE " + PersonalContract.PersonalEntry.TABLE_NAME + " ("
                + PersonalContract.PersonalEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PersonalContract.PersonalEntry.NOMBRES + " TEXT NOT NULL,"
                + PersonalContract.PersonalEntry.PUESTO + " TEXT NOT NULL,"
                + PersonalContract.PersonalEntry.SALARIO + " REAL NOT NULL,"
                + PersonalContract.PersonalEntry.TELEFONO + " TEXT NOT NULL,"
                + PersonalContract.PersonalEntry.EMAIL + " TEXT NOT NULL,"
                + PersonalContract.PersonalEntry.INFORMACION + " TEXT);");

        sqLiteDatabase.execSQL("CREATE TABLE " + ClienteContract.ClienteEntry.TABLE_NAME + " ("
                + ClienteContract.ClienteEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ClienteContract.ClienteEntry.NOMBRES + " TEXT NOT NULL,"
                + ClienteContract.ClienteEntry.TELEFONO + " TEXT NOT NULL,"
                + ClienteContract.ClienteEntry.EMAIL + " TEXT NOT NULL,"
                + ClienteContract.ClienteEntry.DIRECCION + " TEXT NOT NULL,"
                + ClienteContract.ClienteEntry.INFORMACION + " TEXT);");

        sqLiteDatabase.execSQL("CREATE TABLE " + VentaCabContract.VentaCabEntry.TABLE_NAME + " ("
                + VentaCabContract.VentaCabEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + VentaCabContract.VentaCabEntry.FECHA + " TEXT NOT NULL,"
                + VentaCabContract.VentaCabEntry.CLIENTE + " TEXT NOT NULL,"
                + VentaCabContract.VentaCabEntry.FORMA_PAGO + " TEXT NOT NULL,"
                + VentaCabContract.VentaCabEntry.TOTAL + " REAL NOT NULL);");

        sqLiteDatabase.execSQL("CREATE TABLE " + VentaDetContract.VentaDetEntry.TABLE_NAME + " ("
                + VentaDetContract.VentaDetEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + VentaDetContract.VentaDetEntry.VENTA_CAB_ID + " INTEGER NOT NULL,"
                + VentaDetContract.VentaDetEntry.PRODUCTO_ID + " INTEGER NOT NULL,"
                + VentaDetContract.VentaDetEntry.CANTIDAD + " INTEGER NOT NULL,"
                + VentaDetContract.VentaDetEntry.SUBTOTAL + " REAL NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ProductoContract.ProductoEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + UnidadContract.UnidadEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CategoriaProdContract.CategoriaProdEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PersonalContract.PersonalEntry.TABLE_NAME);
        onCreate(db);
    }

}
