package com.appge.appge.DataBase;

import android.provider.BaseColumns;

public class NegocioContract {
    public static abstract class NegocioEntry implements BaseColumns {
        public static final String TABLE_NAME = "negocio";

        public static final String NOMBRE_NEGOCIO = "neg_nombre_negocio";
        public static final String NOMBRE_CONTACTO = "neg_nombre_contacto";
        public static final String EMAIL = "neg_email";
        public static final String TELEFONO = "neg_telefono";
        public static final String INFORMACION = "neg_info";
    }
}
