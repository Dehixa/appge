package com.appge.appge;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.appge.appge.Controller.PersonalController;
import com.appge.appge.Model.Personal;

public class FormPersonalActivity extends AppCompatActivity {

    TextInputEditText nombres;
    TextInputEditText puesto;
    TextInputEditText salario;
    TextInputEditText telefono;
    TextInputEditText email;
    TextInputEditText info;

    TextInputLayout layout_nombres;
    TextInputLayout layout_puesto;
    TextInputLayout layout_salario;
    TextInputLayout layout_telefono;
    TextInputLayout layout_email;
    TextInputLayout layout_info;

    private int personalID;
    private String opcion;
    private PersonalController controller;
    private Menu menu;
    private MenuItem item_edit;
    private MenuItem item_delete;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_personal);
        toolbar = (Toolbar) findViewById(R.id.toolbar4);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        controller = new PersonalController(this);
        personalID = 0;
        opcion = "ver";

        nombres = findViewById(R.id.editText_nombres_pers2);
        puesto = findViewById(R.id.editText_puesto_pers2);
        salario = findViewById(R.id.editText_salario_pers2);
        telefono = findViewById(R.id.editText_telefono_pers2);
        email = findViewById(R.id.editText_email_pers2);
        info = findViewById(R.id.editText_info_pers2);

        // Guardamos TextLayout (Caja donde esta el EditText)
        layout_nombres = findViewById(R.id.text_input_nombres_pers2);
        layout_puesto = findViewById(R.id.text_input_puesto_pers2);
        layout_salario = findViewById(R.id.text_input_salario_pers2);
        layout_telefono = findViewById(R.id.text_input_telefono_pers2);
        layout_email = findViewById(R.id.text_input_email_pers2);
        layout_info = findViewById(R.id.text_input_info_pers2);

        hideKeyboard();

        toolbar.setTitle("Detalle del Personal");
        personalID = getIntent().getExtras().getInt("personalID");
        cargarPersonal();

        deshabilitarCampos();

    }

    @Override public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_form_editar2, this.menu);

        item_edit = menu.getItem(0);
        item_delete = menu.getItem(1);

        return super.onCreateOptionsMenu(this.menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_2:
                if (opcion.equals("ver")){
                    opcion = "editar";
                    habilitarCampos();
                    toolbar.setTitle("Editar Personal");
                    item_edit.setIcon(getResources().getDrawable(R.drawable.ic_check_white));
                } else if (opcion.equals("editar")){
                    opcion = "ver";
                    toolbar.setTitle("Detalle del Personal");
                    deshabilitarCampos();
                    item_edit.setIcon(getResources().getDrawable(R.drawable.ic_edit_white));

                    if (validar()) {
                        // Se edita en la base de datos
                        Personal personal = new Personal();
                        personal.setCodigo(personalID);
                        personal.setNombres(nombres.getText().toString());
                        personal.setPuesto(puesto.getText().toString());
                        personal.setSalario(Double.valueOf(salario.getText().toString()));
                        personal.setTelefono(telefono.getText().toString());
                        personal.setEmail(email.getText().toString());
                        personal.setInfo(info.getText().toString());

                        long rows = controller.editPersonal(personal);

                        if (rows > 0)
                            Toast.makeText(this, "Empleado modificado exitosamente.", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(this, "Error al modificar.", Toast.LENGTH_SHORT).show();
                    }
                }

                return true;

            case R.id.action_delete_2:
                if (opcion.equals("ver")){
                    // borrar registro
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
                    String mensaje = "¿Desea eliminar este empleado?";
                    dlgAlert.setMessage(mensaje);
                    dlgAlert.setTitle("Eliminar");
                    dlgAlert.setPositiveButton("Eliminar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    eliminarPersonal();
                                    onBackPressed();
                                }
                            });
                    dlgAlert.setNegativeButton("Cancelar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    dlgAlert.create().show();

                } else if (opcion.equals("editar")){
                    Toast.makeText(this, "Debe guardar primero las modificaciones realizadas.", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void hideKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void cargarPersonal(){
        Personal personal = controller.getPersonalByID(personalID);
        nombres.setText(personal.getNombres());
        puesto.setText(personal.getPuesto());
        salario.setText(String.valueOf(personal.getSalario()));
        telefono.setText(personal.getTelefono());
        email.setText(personal.getEmail());
        info.setText(personal.getInfo());

        nombres.requestFocus();
    }

    private void eliminarPersonal(){
        long rowDelete = controller.deletePersonal(personalID);
        if (rowDelete > 0)
            Toast.makeText(this, "Empleado eliminado.", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Error al eliminar.", Toast.LENGTH_SHORT).show();
    }

    private void habilitarCampos(){
        nombres.setEnabled(true);
        puesto.setEnabled(true);
        salario.setEnabled(true);
        telefono.setEnabled(true);
        email.setEnabled(true);
        info.setEnabled(true);

        nombres.requestFocus();
    }

    private void deshabilitarCampos(){
        nombres.setEnabled(false);
        puesto.setEnabled(false);
        salario.setEnabled(false);
        telefono.setEnabled(false);
        email.setEnabled(false);
        info.setEnabled(false);

        nombres.requestFocus();
    }

    private boolean validar() {
        boolean valido = true;

        // Comprueba que el campo de Nombres y Apellidos no este vacio.
        if (TextUtils.isEmpty(nombres.getText())) {
            layout_nombres.setError("Debe ingresar Nombres y Apellidos.");
            layout_nombres.setErrorEnabled(true);
            valido = false;
        }else{
            layout_nombres.setErrorEnabled(false);
        }

        // Comprueba que el campo de Puesto no este vacio.
        if (TextUtils.isEmpty(puesto.getText())) {
            layout_puesto.setError("Debe ingresar el Puesto de Trabajo.");
            layout_puesto.setErrorEnabled(true);
            valido = false;
        }else{
            layout_puesto.setErrorEnabled(false);
        }

        // Comprueba que el campo de Salario no este vacio.
        if (TextUtils.isEmpty(salario.getText())) {
            layout_salario.setError("Debe ingresar un Salario.");
            layout_salario.setErrorEnabled(true);
            valido = false;
        }else{
            layout_salario.setErrorEnabled(false);
        }

        // Comprueba que el campo de Telefono no este vacio.
        if (TextUtils.isEmpty(telefono.getText())) {
            layout_telefono.setError("Debe ingresar un Número telefónico.");
            layout_telefono.setErrorEnabled(true);
            valido = false;
        }else{
            layout_telefono.setErrorEnabled(false);
        }
        // Comprueba que el campo de Email no este vacio o que contenga un @.
        if (TextUtils.isEmpty(email.getText())) {
            layout_email.setError("Debe ingresar un Email.");
            layout_email.setErrorEnabled(true);
            valido = false;
        }else if (TextUtils.indexOf(email.getText(), "@") == -1){
            layout_email.setError("El email debe contener un @.");
            layout_email.setErrorEnabled(true);
            valido = false;
        }else{
            layout_email.setErrorEnabled(false);
        }

        return valido;
    }

}
