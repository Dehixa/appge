package com.appge.appge;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appge.appge.Adapter.AutocompleteProdAdapter;
import com.appge.appge.Adapter.ProductoAdapter;
import com.appge.appge.Controller.ProductoController;
import com.appge.appge.Dialog.BottomSheetFragmentInventario;
import com.appge.appge.Model.Producto;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InventarioFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InventarioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InventarioFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ListView lista;
    private ProductoAdapter adapter;
    private int codigo;
    private AutoCompleteTextView text_buscar;

    public InventarioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InventarioFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InventarioFragment newInstance(String param1, String param2) {
        InventarioFragment fragment = new InventarioFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inventario, container, false);

        ((Main2Activity) getActivity()).getSupportActionBar().setSubtitle("Inventario");

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetFragmentInventario fragment = new BottomSheetFragmentInventario();
                fragment.show(getFragmentManager(), "TAG");
            }
        });

        text_buscar = view.findViewById(R.id.autoText_buscar_prod);
        TextView mensaje = view.findViewById(R.id.text_inventario_vacio);
        lista = view.findViewById(R.id.listview_productos);

        ProductoController controller = new ProductoController(getContext());
        ArrayList<Producto> productos = controller.listarProductos();
        adapter = new ProductoAdapter(getContext(), R.layout.row_productos, productos);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Producto item = (Producto) lista.getItemAtPosition(position);

                codigo = item.getCodigo();

                Intent intent = new Intent(getContext(), FormProductoActivity.class);
                intent.putExtra("opcion", "ver");
                intent.putExtra("productoID", codigo);
                startActivity(intent);

            }
        });

        AutocompleteProdAdapter adapterSearch = new AutocompleteProdAdapter(getContext(),
                R.layout.row_prod_autocomplete, productos);

        text_buscar.setAdapter(adapterSearch);
        text_buscar.setOnItemClickListener(onItemClickListener);

        if (productos.size() > 0){
            mensaje.setVisibility(View.INVISIBLE);
        } else {
            mensaje.setVisibility(View.VISIBLE);
        }

        hideKeyboard();

        return view;
    }

    public ProductoAdapter getAdapter(){
        return adapter;
    }

    public void hideKeyboard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private AdapterView.OnItemClickListener onItemClickListener =
            new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    Producto p = (Producto) adapterView.getItemAtPosition(i);

                    text_buscar.setText(p.getNombre());

                    Toast.makeText(getActivity(),
                            p.getNombre()
                            , Toast.LENGTH_SHORT).show();
                }
            };

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
