package com.appge.appge.Dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import com.appge.appge.Controller.CategoriaProdController;
import com.appge.appge.Controller.UnidadController;
import com.appge.appge.Model.CategoriaProd;
import com.appge.appge.Model.Unidad;
import com.appge.appge.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ElementoDialog extends DialogFragment implements DialogInterface.OnClickListener {

    private TextInputEditText editText;
    private TextInputLayout layout_editText;
    private String text;
    String tipoDialog;

    public interface DialogListener {
        void onCloseDialog(String message);
        void onCancelDialog();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        DialogInterface.OnClickListener btnClose = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DialogListener dialogListener = (DialogListener) getActivity();

                dialogListener.onCancelDialog();
            }
        };

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_elemento, null);

        builder.setView(dialogView);

        tipoDialog = getArguments().getString("tipoDialog");

        editText = dialogView.findViewById(R.id.editText_nombre_elemento);
        layout_editText = dialogView.findViewById(R.id.text_input_nombre_elemento);

        if (tipoDialog.equals("unidad")){
            layout_editText.setHint("Unidad");
        }
        else if (tipoDialog.equals("categoria_prod")){
            layout_editText.setHint("Categoría");
        }

        builder.setNegativeButton("Cancelar", btnClose);
        builder.setPositiveButton("Registrar", this);

        return builder.create();

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        ElementoDialog.DialogListener dialogListener = (ElementoDialog.DialogListener) getActivity();

        if (TextUtils.isEmpty(editText.getText())){
            layout_editText.setError("Debe llenar el campo.");
            layout_editText.setErrorEnabled(true);
        } else {
            layout_editText.setErrorEnabled(false);
            int newId = 0;
            if (tipoDialog.equals("unidad")){
                // Se guarda en la base de datos
                Unidad unidad = new Unidad();
                unidad.setNombre(editText.getText().toString());

                UnidadController unidadController = new UnidadController(getContext());
                newId = (int) unidadController.addUnidad(unidad);
            }
            else if (tipoDialog.equals("categoria_prod")){
                CategoriaProd categoria = new CategoriaProd();
                categoria.setNombre(editText.getText().toString());

                CategoriaProdController controller = new CategoriaProdController(getContext());
                newId = (int) controller.addCategoria(categoria);
            }

            // Se envia el valor del campo al formulario
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("tipoDialog", tipoDialog);
                jsonObject.put("valorID", newId);
                jsonObject.put("valor", editText.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            dialogListener.onCloseDialog(jsonObject.toString());
        }

        this.dismiss();

    }
}