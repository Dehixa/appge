package com.appge.appge.Dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appge.appge.Controller.PersonalController;
import com.appge.appge.Model.Personal;
import com.appge.appge.R;

public class RegistroPersonalDialog extends DialogFragment {

    TextInputEditText nombres;
    TextInputEditText puesto;
    TextInputEditText salario;
    TextInputEditText telefono;
    TextInputEditText email;
    TextInputEditText info;

    TextInputLayout layout_nombres;
    TextInputLayout layout_puesto;
    TextInputLayout layout_salario;
    TextInputLayout layout_telefono;
    TextInputLayout layout_email;
    TextInputLayout layout_info;

    public static final String TAG = RegistroPersonalDialog.class.getSimpleName();

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        View content = LayoutInflater.from(getContext()).inflate(R.layout.dialog_registro_personal, null);

        setupContent(content);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(content)
                .setNegativeButton(Html.fromHtml("<font>CANCELAR</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setCancelable(true)
                .setPositiveButton(Html.fromHtml("<font>REGISTRAR</font>"), null);

        AlertDialog alertDialog = builder.create();

        return alertDialog;

    }

    // Este método permite manejar la implementación del botón POSITIVO del Dialogo de otra forma,
    // de modo que primero validará los datos y luego procedera al registro.
    @Override
    public void onStart() {
        super.onStart();
        Button positiveButton = ((AlertDialog) getDialog()).getButton(Dialog.BUTTON_POSITIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    // Se guarda en la base de datos
                    Personal personal = new Personal();
                    personal.setNombres(nombres.getText().toString());
                    personal.setPuesto(puesto.getText().toString());
                    personal.setSalario(Double.valueOf(salario.getText().toString()));
                    personal.setTelefono(telefono.getText().toString());
                    personal.setEmail(email.getText().toString());
                    personal.setInfo(info.getText().toString());

                    PersonalController controller = new PersonalController(getContext());

                    long id = controller.addPersonal(personal);

                    if (id > 0)
                        Toast.makeText(getContext(), "Personal registrado exitosamente.", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getContext(), "Error al registrar.", Toast.LENGTH_SHORT).show();

                    getDialog().dismiss();

                }
            }
        });
    }

    // Validacion de campos de resgistro
    private boolean validate() {
        boolean valido = true;

        // Comprueba que el campo de Nombres y Apellidos no este vacio.
        if (TextUtils.isEmpty(nombres.getText())) {
            layout_nombres.setError("Debe ingresar Nombres y Apellidos.");
            layout_nombres.setErrorEnabled(true);
            valido = false;
        }else{
            layout_nombres.setErrorEnabled(false);
        }

        // Comprueba que el campo de Puesto no este vacio.
        if (TextUtils.isEmpty(puesto.getText())) {
            layout_puesto.setError("Debe ingresar el Puesto de Trabajo.");
            layout_puesto.setErrorEnabled(true);
            valido = false;
        }else{
            layout_puesto.setErrorEnabled(false);
        }

        // Comprueba que el campo de Salario no este vacio.
        if (TextUtils.isEmpty(salario.getText())) {
            layout_salario.setError("Debe ingresar un Salario.");
            layout_salario.setErrorEnabled(true);
            valido = false;
        }else{
            layout_salario.setErrorEnabled(false);
        }

        // Comprueba que el campo de Telefono no este vacio.
        if (TextUtils.isEmpty(telefono.getText())) {
            layout_telefono.setError("Debe ingresar un Número telefónico.");
            layout_telefono.setErrorEnabled(true);
            valido = false;
        }else{
            layout_telefono.setErrorEnabled(false);
        }
        // Comprueba que el campo de Email no este vacio o que contenga un @.
        if (TextUtils.isEmpty(email.getText())) {
            layout_email.setError("Debe ingresar un Email.");
            layout_email.setErrorEnabled(true);
            valido = false;
        }else if (TextUtils.indexOf(email.getText(), "@") == -1){
            layout_email.setError("El email debe contener un @.");
            layout_email.setErrorEnabled(true);
            valido = false;
        }else{
            layout_email.setErrorEnabled(false);
        }

        return valido;
    }

    // Reconoce los elementos del activity
    private void setupContent(View content) {
        nombres =  content.findViewById(R.id.editText_nombres_pers);
        puesto = content.findViewById(R.id.editText_puesto_pers);
        salario = content.findViewById(R.id.editText_salario_pers);
        telefono = content.findViewById(R.id.editText_telefono_pers);
        email = content.findViewById(R.id.editText_email_pers);
        info = content.findViewById(R.id.editText_info_pers);

        // Guardamos TextLayout (Caja donde esta el EditText)
        layout_nombres = content.findViewById(R.id.text_input_nombres_pers);
        layout_puesto = content.findViewById(R.id.text_input_puesto_pers);
        layout_salario = content.findViewById(R.id.text_input_salario_pers);
        layout_telefono = content.findViewById(R.id.text_input_telefono_pers);
        layout_email = content.findViewById(R.id.text_input_email_pers);
        layout_info = content.findViewById(R.id.text_input_info_pers);

    }

}
