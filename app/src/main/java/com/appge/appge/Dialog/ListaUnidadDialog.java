package com.appge.appge.Dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.appge.appge.Adapter.UnidadAdapter;
import com.appge.appge.Controller.UnidadController;
import com.appge.appge.Listener.DialogUnidadListener;
import com.appge.appge.Model.Unidad;
import com.appge.appge.R;

import java.util.ArrayList;

public class ListaUnidadDialog extends DialogFragment {

    private AlertDialog alertDialog;
    private ListView lista;
    private int codigo;
    private String nombre;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_lista, null);
        View headerView = (View) inflater.inflate(R.layout.dialog_lista_header, null);
        TextView txtDialogTitle = (TextView) headerView.findViewById(R.id.txtDialogTitle);
        FloatingActionButton fab = (FloatingActionButton) headerView.findViewById(R.id.fab_add_elemento);
        lista = dialogView.findViewById(R.id.listview_lista);

        txtDialogTitle.setText("Seleccione una unidad");

        UnidadController unidadController = new UnidadController(getContext());

        UnidadAdapter unidadAdapter = new UnidadAdapter(getContext(), R.id.listview_lista, unidadController.listarUnidad());

        lista.setAdapter(unidadAdapter);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCustomTitle(headerView);
        builder.setView(dialogView);

        alertDialog = builder.create();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putString("tipoDialog", "unidad");

                ElementoDialog elementoDialog = new ElementoDialog();
                elementoDialog.setArguments(bundle);
                elementoDialog.show(getFragmentManager(), "TAG");

                alertDialog.dismiss();
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Unidad item = (Unidad) lista.getItemAtPosition(position);

                codigo = item.getCodigo();
                nombre = item.getNombre();

                DialogUnidadListener activity = (DialogUnidadListener) getActivity();
                activity.onReturnUnidadValue(codigo, nombre);

                alertDialog.dismiss();

            }
        });

        return alertDialog;

    }
}