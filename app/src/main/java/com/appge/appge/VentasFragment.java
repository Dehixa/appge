package com.appge.appge;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.appge.appge.Adapter.AutocompleteProdAdapter;
import com.appge.appge.Adapter.VentasAdapter;
import com.appge.appge.Controller.ProductoController;
import com.appge.appge.Controller.VentaCabController;
import com.appge.appge.Controller.VentaDetController;
import com.appge.appge.Listener.VentasListener;
import com.appge.appge.Model.Producto;
import com.appge.appge.Model.VentaCabecera;
import com.appge.appge.Model.VentaDetalle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VentasFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VentasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VentasFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private LinearLayout layout_ventas;
    private ListView lista_ventas;
    private TextView precio_total;
    private TextInputEditText cliente;
    private TextInputLayout layout_cliente;
    private RadioGroup forma;
    private TextView mensaje;

    private String formaPago;
    private ArrayList<Producto> items;
    private int cod;
    private double total;
    public static HashMap<Integer, Double> PRECIO_TOTAL = new HashMap<Integer, Double>();
    public static HashMap<Integer, Integer> CANTIDADES = new HashMap<Integer, Integer>();

    private VentaCabController ventaCabController;
    private VentaDetController ventaDetController;
    private ProductoController productoController;

    private OnFragmentInteractionListener mListener;

    public VentasFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VentasFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VentasFragment newInstance(String param1, String param2) {
        VentasFragment fragment = new VentasFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        PRECIO_TOTAL.clear();
        CANTIDADES.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Limpiar el precio Total
        PRECIO_TOTAL.clear();
        CANTIDADES.clear();
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ventas, container, false);

        ((Main2Activity) getActivity()).getSupportActionBar().setSubtitle("Ventas");

        layout_cliente = view.findViewById(R.id.text_input_cli_ventas);
        cliente = view.findViewById(R.id.editText_cli_ventas);

        layout_ventas = view.findViewById(R.id.layout_ventas);
        layout_ventas.setVisibility(View.INVISIBLE);

        lista_ventas = view.findViewById(R.id.listview_ventas);
        items = new ArrayList<Producto>();

        mensaje = view.findViewById(R.id.text_ventas_indicaciones);
        precio_total = view.findViewById(R.id.text_total_ventas);


        // Llenar Autocomplete
        ProductoController controller = new ProductoController(getContext());
        ArrayList<Producto> productos = controller.listarProductos();
        AutocompleteProdAdapter adapter = new AutocompleteProdAdapter(getContext(), R.layout.row_prod_autocomplete, productos);

        final AutoCompleteTextView autocomplete = view.findViewById(R.id.autoText_add_producto);
        autocomplete.setThreshold(1);
        autocomplete.setAdapter(adapter);

        cod = 0;
        total = 0.0;

        precio_total.setText(String.valueOf(total));

        autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                autocomplete.setText("");
                mensaje.setVisibility(View.INVISIBLE);
                layout_ventas.setVisibility(View.VISIBLE);
                precio_total.setText(String.valueOf("0.0"));
                Producto producto = (Producto) adapterView.getItemAtPosition(i);
                items.add(producto);
                VentasAdapter adapterV = new VentasAdapter(getContext(), R.layout.row_prod_ventas, items, new VentasListener() {
                    @Override
                    public void onReturnPrice() {
                        double aux = 0.0;
                        for (HashMap.Entry<Integer,Double> entry : PRECIO_TOTAL.entrySet()) {
                            aux += entry.getValue();
                        }

                        total = aux;
                        precio_total.setText(String.valueOf(total));
                    }
                });
                lista_ventas.setAdapter(adapterV);
            }
        });

        formaPago = "";
        forma = view.findViewById(R.id.opciones_pago_ventas);

        ventaCabController = new VentaCabController(getContext());
        ventaDetController = new VentaDetController(getContext());
        productoController = new ProductoController(getContext());

        ImageButton aceptar = view.findViewById(R.id.btn_check_ventas);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (forma.getCheckedRadioButtonId() == R.id.efectivo_ventas) {
                    formaPago = "Efectivo";
                } else if (forma.getCheckedRadioButtonId() == R.id.tarjeta_ventas) {
                    formaPago = "Tarjeta";
                }

                if (validar()){
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd / MM / yyyy", Locale.getDefault());
                    Date date = new Date();
                    String fecha = dateFormat.format(date);

                    int codigo = ventaCabController.getNewCodigo();
                    VentaCabecera ventaCabecera = new VentaCabecera();
                    ventaCabecera.setCodigo(codigo);
                    ventaCabecera.setFecha(fecha);
                    ventaCabecera.setCliente(cliente.getText().toString());
                    ventaCabecera.setFormaPago(formaPago);
                    ventaCabecera.setTotal(total);

                    long id = ventaCabController.addVenta(ventaCabecera);

                    if (id > 0)
                        Toast.makeText(getContext(), "Venta registrada exitosamente.", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getContext(), "Error al registrar.", Toast.LENGTH_SHORT).show();

                    System.out.println("****************************************************************+");
                    System.out.println("codigo: " + codigo);
                    System.out.println("fecha: " + fecha);
                    System.out.println("cliente: " + cliente.getText().toString());
                    System.out.println("total: " + PRECIO_TOTAL);
                    System.out.println("cantidades: " + CANTIDADES);
                    System.out.println("pago: " + formaPago);
                    System.out.println("****************************************************************+");

                    for (HashMap.Entry<Integer,Double> entry : PRECIO_TOTAL.entrySet()) {
                        VentaDetalle ventaDetalle = new VentaDetalle();
                        ventaDetalle.setCodigoVentaCab(codigo);
                        ventaDetalle.setCodigoProducto(entry.getKey());
                        ventaDetalle.setCantidad(CANTIDADES.get(entry.getKey()));
                        ventaDetalle.setSubtotal(entry.getValue());

                        Producto producto = productoController.getProductoByID(entry.getKey());
                        int resto = producto.getCantidad() - CANTIDADES.get(entry.getKey());

                        long rows = productoController.editCantidad(entry.getKey(), resto);
                        long idV = ventaDetController.addVentaDet(ventaDetalle);

                        if (idV > 0 && rows > 0)
                            System.out.println("Registrado exitosamente");
                        else
                            System.out.println("Error al registrar");
                    }

                    limpiarVentana();
                } else {
                    Toast.makeText(getContext(), "Todos los campos deben estar llenados.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ImageButton anular = view.findViewById(R.id.btn_delete_ventas);
        anular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(getContext());
                String mensaje = "¿Desea anular la venta?";
                dlgAlert.setMessage(mensaje);
                dlgAlert.setTitle("Anular Venta");
                dlgAlert.setPositiveButton("SI",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                limpiarVentana();
                                Toast.makeText(getContext(), "Venta Anulada", Toast.LENGTH_SHORT).show();
                            }
                        });
                dlgAlert.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                dlgAlert.create().show();
            }
        });

        return view;
    }

    private void limpiarVentana(){
        PRECIO_TOTAL.clear();
        CANTIDADES.clear();
        forma.check(R.id.efectivo_ventas);
        cliente.setText("");
        items.clear();
        VentasAdapter adapterV = new VentasAdapter(getContext(), R.layout.row_prod_ventas, items, null);
        lista_ventas.setAdapter(adapterV);
        layout_ventas.setVisibility(View.INVISIBLE);
        mensaje.setVisibility(View.VISIBLE);
    }

    private boolean validar() {
        boolean valido = true;

        if (TextUtils.isEmpty(cliente.getText())) {
            layout_cliente.setError("Debe ingresar un Cliente");
            layout_cliente.setErrorEnabled(true);
            valido = false;
        }else{
            layout_cliente.setErrorEnabled(false);
        }

        if (PRECIO_TOTAL.isEmpty()){
            valido = false;
        }

        if (CANTIDADES.isEmpty()){
            valido = false;
        }

        return valido;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
