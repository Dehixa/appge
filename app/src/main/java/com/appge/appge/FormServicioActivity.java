package com.appge.appge;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

public class FormServicioActivity extends AppCompatActivity {

    String opcion;
    Menu menu;
    MenuItem item_edit;
    MenuItem item_add_delete;

    TextInputLayout layout_codigo;
    TextInputLayout layout_nombre;
    TextInputLayout layout_costo_inversion;
    TextInputLayout layout_precio_venta;
    TextInputLayout layout_info;

    TextInputEditText codigo;
    TextInputEditText nombre;
    TextInputEditText costo_inversion;
    TextInputEditText precio_venta;
    TextInputEditText info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_servicio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        opcion = getIntent().getExtras().getString("opcion");

        hideKeyboard();

        codigo = findViewById(R.id.editText_codigo_serv);
        nombre = findViewById(R.id.editText_nombre_serv);
        costo_inversion = findViewById(R.id.editText_costo_inversion_serv);
        precio_venta = findViewById(R.id.editText_precio_venta_serv);
        info = findViewById(R.id.editText_info_serv);

        if (opcion.equals("editar")){
            toolbar.setTitle("Agregar Producto");
            codigo.setEnabled(true);
            nombre.setEnabled(true);
            costo_inversion.setEnabled(true);
            precio_venta.setEnabled(true);
            info.setEnabled(true);

        } else if (opcion.equals("ver")){
            toolbar.setTitle("Detalle del Servicio");
            codigo.setEnabled(false);
            nombre.setEnabled(false);
            costo_inversion.setEnabled(false);
            precio_venta.setEnabled(false);
            info.setEnabled(false);
        }

    }

    @Override public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_form_editar1, this.menu);

        item_edit = menu.getItem(0);
        item_add_delete = menu.getItem(1);

        if (opcion.equals("editar")){
            item_edit.setVisible(false);
            item_edit.setEnabled(false);
            item_add_delete.setIcon(getResources().getDrawable(R.drawable.ic_check_white));

        } else if (opcion.equals("ver")){
            item_edit.setVisible(true);
            item_edit.setEnabled(true);
            item_add_delete.setIcon(getResources().getDrawable(R.drawable.ic_delete_white));
        }

        return super.onCreateOptionsMenu(this.menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_or_delete_1:
                //
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void hideKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


}
