package com.appge.appge.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appge.appge.DataBase.DataBase;
import com.appge.appge.DataBase.UnidadContract;
import com.appge.appge.Model.Unidad;

import java.util.ArrayList;

public class UnidadController {

    Context context;
    DataBase dataBase;
    SQLiteDatabase dbWriter;
    SQLiteDatabase dbReader;

    public UnidadController (Context context){
        this.context = context;
        dataBase = new DataBase(context);
        dbWriter = null;
        dbReader = null;
    }

    // Registrar una nueva unidad
    public long addUnidad(Unidad unidad){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(UnidadContract.UnidadEntry.NOMBRE, unidad.getNombre());

        // Insertar
        dbWriter = dataBase.getWritableDatabase();
        long newId = dbWriter.insert(UnidadContract.UnidadEntry.TABLE_NAME, null, values);
        dbWriter.close();

        return newId;
    }

    // Listar todas las unidades
    public ArrayList<Unidad> listarUnidad(){
        dbReader = dataBase.getReadableDatabase();
        String query = ("SELECT * FROM " + UnidadContract.UnidadEntry.TABLE_NAME + ";");
        Cursor c = dbReader.rawQuery(query, null);

        ArrayList<Unidad> unidades = new ArrayList<Unidad>();

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            Unidad unidad;
            do {
                unidad = new Unidad();
                unidad.setCodigo(c.getInt(c.getColumnIndex(UnidadContract.UnidadEntry._ID)));
                unidad.setNombre(c.getString(c.getColumnIndex(UnidadContract.UnidadEntry.NOMBRE)));
                unidades.add(unidad);
                unidad = null;
            } while(c.moveToNext());
        }
        dbReader.close();

        return unidades;
    }

    // buscar Unidad por ID
    public Unidad getUnidadByID(int id){
        dbReader = dataBase.getReadableDatabase();
        String query = "SELECT * FROM " + UnidadContract.UnidadEntry.TABLE_NAME + " WHERE " + UnidadContract.UnidadEntry._ID + " = " + id + ";";
        Cursor c = dbReader.rawQuery(query, null);

        if (c != null) { c.moveToFirst(); }

        Unidad unidad = new Unidad();
        unidad.setCodigo(c.getInt(c.getColumnIndex(UnidadContract.UnidadEntry._ID)));
        unidad.setNombre(c.getString(c.getColumnIndex(UnidadContract.UnidadEntry.NOMBRE)));

        return unidad;
    }
}
