package com.appge.appge.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appge.appge.DataBase.ClienteContract;
import com.appge.appge.DataBase.DataBase;
import com.appge.appge.Model.Cliente;

import java.util.ArrayList;

public class ClienteController {
    Context context;
    DataBase dataBase;
    SQLiteDatabase dbWriter;
    SQLiteDatabase dbReader;

    public ClienteController(Context context) {
        this.context = context;
        dataBase = new DataBase(context);
        dbWriter = null;
        dbReader = null;
    }

    // Registrar un nuevo cliente
    public long addCliente(Cliente cliente) {
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(ClienteContract.ClienteEntry.NOMBRES, cliente.getNombres());
        values.put(ClienteContract.ClienteEntry.TELEFONO, cliente.getTelefono());
        values.put(ClienteContract.ClienteEntry.EMAIL, cliente.getEmail());
        values.put(ClienteContract.ClienteEntry.DIRECCION, cliente.getDireccion());
        values.put(ClienteContract.ClienteEntry.INFORMACION, cliente.getInfo());

        // insertar
        dbWriter = dataBase.getWritableDatabase();
        long newId = dbWriter.insert(ClienteContract.ClienteEntry.TABLE_NAME, null, values);
        dbWriter.close();

        return newId;
    }

    // Modificar un personal
    public long editCliente(Cliente cliente){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(ClienteContract.ClienteEntry.NOMBRES, cliente.getNombres());
        values.put(ClienteContract.ClienteEntry.TELEFONO, cliente.getTelefono());
        values.put(ClienteContract.ClienteEntry.EMAIL, cliente.getEmail());
        values.put(ClienteContract.ClienteEntry.DIRECCION, cliente.getDireccion());
        values.put(ClienteContract.ClienteEntry.INFORMACION, cliente.getInfo());

        // Modificar
        dbWriter = dataBase.getWritableDatabase();
        long rows = dbWriter.update(ClienteContract.ClienteEntry.TABLE_NAME, values, ClienteContract.ClienteEntry._ID + "='" + cliente.getCodigo() + "'", null);
        dbWriter.close();

        return rows;
    }

    // listar todos los clientes
    public ArrayList<Cliente> listarClientes() {
        dbReader = dataBase.getReadableDatabase();
        String query = "SELECT * FROM " + ClienteContract.ClienteEntry.TABLE_NAME + ";";
        Cursor c = dbReader.rawQuery(query, null);

        ArrayList<Cliente> clientes = new ArrayList<Cliente>();

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                Cliente cliente = new Cliente();
                cliente.setCodigo((int) c.getLong(c.getColumnIndex(ClienteContract.ClienteEntry._ID)));
                cliente.setNombres(c.getString(c.getColumnIndex(ClienteContract.ClienteEntry.NOMBRES)));
                cliente.setTelefono(c.getString(c.getColumnIndex(ClienteContract.ClienteEntry.TELEFONO)));
                cliente.setEmail(c.getString(c.getColumnIndex(ClienteContract.ClienteEntry.EMAIL)));
                cliente.setDireccion(c.getString(c.getColumnIndex(ClienteContract.ClienteEntry.DIRECCION)));
                cliente.setInfo(c.getString(c.getColumnIndex(ClienteContract.ClienteEntry.INFORMACION)));

                clientes.add(cliente);
                cliente = null;
            } while (c.moveToNext());
        }
        dbReader.close();

        return clientes;
    }

    // buscar cliente por ID
    public Cliente getClienteByID(int id){
        dbReader = dataBase.getReadableDatabase();
        String query = "SELECT * FROM " + ClienteContract.ClienteEntry.TABLE_NAME + " WHERE " + ClienteContract.ClienteEntry._ID + " = " + id + ";";
        Cursor c = dbReader.rawQuery(query, null);

        if (c != null) { c.moveToFirst(); }

        Cliente cliente = new Cliente();
        cliente.setCodigo((int) c.getLong(c.getColumnIndex(ClienteContract.ClienteEntry._ID)));
        cliente.setNombres(c.getString(c.getColumnIndex(ClienteContract.ClienteEntry.NOMBRES)));
        cliente.setTelefono(c.getString(c.getColumnIndex(ClienteContract.ClienteEntry.TELEFONO)));
        cliente.setEmail(c.getString(c.getColumnIndex(ClienteContract.ClienteEntry.EMAIL)));
        cliente.setDireccion(c.getString(c.getColumnIndex(ClienteContract.ClienteEntry.DIRECCION)));
        cliente.setInfo(c.getString(c.getColumnIndex(ClienteContract.ClienteEntry.INFORMACION)));

        return cliente;
    }

    // Eliminar cliente
    public long deleteCliente(int id){
        // Eliminar
        dbWriter = dataBase.getWritableDatabase();
        // db.delete("comments", "user=?", args);
        long rows = dbWriter.delete(ClienteContract.ClienteEntry.TABLE_NAME, ClienteContract.ClienteEntry._ID + "='" + id + "'", null);
        dbWriter.close();

        return rows;
    }

}
