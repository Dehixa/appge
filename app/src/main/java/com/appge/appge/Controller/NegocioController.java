package com.appge.appge.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appge.appge.DataBase.DataBase;
import com.appge.appge.DataBase.NegocioContract;
import com.appge.appge.Model.Negocio;

public class NegocioController {
    Context context;
    DataBase dataBase;
    SQLiteDatabase dbWriter;
    SQLiteDatabase dbReader;

    public NegocioController(Context context) {
        this.context = context;
        dataBase = new DataBase(context);
        dbWriter = null;
        dbReader = null;
    }

    // Registrar un nuevo negocio
    public long addNegocio(Negocio negocio) {
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(NegocioContract.NegocioEntry.NOMBRE_NEGOCIO, negocio.getNombreNegocio());
        values.put(NegocioContract.NegocioEntry.NOMBRE_CONTACTO, negocio.getNombreContacto());
        values.put(NegocioContract.NegocioEntry.TELEFONO, negocio.getTelefono());
        values.put(NegocioContract.NegocioEntry.EMAIL, negocio.getEmail());
        values.put(NegocioContract.NegocioEntry.INFORMACION, negocio.getInfo());

        // insertar
        dbWriter = dataBase.getWritableDatabase();
        long newId = dbWriter.insert(NegocioContract.NegocioEntry.TABLE_NAME, null, values);
        dbWriter.close();

        return newId;
    }

    // Modificar un negocio
    public long editNegocio(Negocio negocio){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(NegocioContract.NegocioEntry.NOMBRE_NEGOCIO, negocio.getNombreNegocio());
        values.put(NegocioContract.NegocioEntry.NOMBRE_CONTACTO, negocio.getNombreContacto());
        values.put(NegocioContract.NegocioEntry.TELEFONO, negocio.getTelefono());
        values.put(NegocioContract.NegocioEntry.EMAIL, negocio.getEmail());
        values.put(NegocioContract.NegocioEntry.INFORMACION, negocio.getInfo());

        // Modificar
        dbWriter = dataBase.getWritableDatabase();
        long rows = dbWriter.update(NegocioContract.NegocioEntry.TABLE_NAME, values, NegocioContract.NegocioEntry._ID + "='" + negocio.getCodigo() + "'", null);
        dbWriter.close();

        return rows;
    }

    // buscar cliente por ID
    public Negocio getNegocioByID(int id){
        dbReader = dataBase.getReadableDatabase();
        String query = "SELECT * FROM " + NegocioContract.NegocioEntry.TABLE_NAME + " WHERE " + NegocioContract.NegocioEntry._ID + " = " + id + ";";
        Cursor c = dbReader.rawQuery(query, null);

        if (c != null) { c.moveToFirst(); }

        Negocio negocio = new Negocio();
        negocio.setCodigo((int) c.getLong(c.getColumnIndex(NegocioContract.NegocioEntry._ID)));
        negocio.setNombreNegocio(c.getString(c.getColumnIndex(NegocioContract.NegocioEntry.NOMBRE_NEGOCIO)));
        negocio.setNombreContacto(c.getString(c.getColumnIndex(NegocioContract.NegocioEntry.NOMBRE_CONTACTO)));
        negocio.setTelefono(c.getString(c.getColumnIndex(NegocioContract.NegocioEntry.TELEFONO)));
        negocio.setEmail(c.getString(c.getColumnIndex(NegocioContract.NegocioEntry.EMAIL)));
        negocio.setInfo(c.getString(c.getColumnIndex(NegocioContract.NegocioEntry.INFORMACION)));

        return negocio;
    }
}
