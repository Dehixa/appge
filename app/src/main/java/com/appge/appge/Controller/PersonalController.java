package com.appge.appge.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appge.appge.DataBase.DataBase;
import com.appge.appge.DataBase.PersonalContract;
import com.appge.appge.Model.Personal;

import java.util.ArrayList;

public class PersonalController {
    Context context;
    DataBase dataBase;
    SQLiteDatabase dbWriter;
    SQLiteDatabase dbReader;

    public PersonalController (Context context){
        this.context = context;
        dataBase = new DataBase(context);
        dbWriter = null;
        dbReader = null;
    }

    // Registrar un nuevo personal
    public long addPersonal(Personal personal){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(PersonalContract.PersonalEntry.NOMBRES, personal.getNombres());
        values.put(PersonalContract.PersonalEntry.PUESTO, personal.getPuesto());
        values.put(PersonalContract.PersonalEntry.SALARIO, personal.getSalario());
        values.put(PersonalContract.PersonalEntry.TELEFONO, personal.getTelefono());
        values.put(PersonalContract.PersonalEntry.EMAIL, personal.getEmail());
        values.put(PersonalContract.PersonalEntry.INFORMACION, personal.getInfo());

        // Insertar
        dbWriter = dataBase.getWritableDatabase();
        long newId = dbWriter.insert(PersonalContract.PersonalEntry.TABLE_NAME, null, values);
        dbWriter.close();

        return newId;
    }

    // Modificar un personal
    public long editPersonal(Personal personal){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(PersonalContract.PersonalEntry.NOMBRES, personal.getNombres());
        values.put(PersonalContract.PersonalEntry.PUESTO, personal.getPuesto());
        values.put(PersonalContract.PersonalEntry.SALARIO, personal.getSalario());
        values.put(PersonalContract.PersonalEntry.TELEFONO, personal.getTelefono());
        values.put(PersonalContract.PersonalEntry.EMAIL, personal.getEmail());
        values.put(PersonalContract.PersonalEntry.INFORMACION, personal.getInfo());

        // Modificar
        dbWriter = dataBase.getWritableDatabase();
        long rows = dbWriter.update(PersonalContract.PersonalEntry.TABLE_NAME, values, PersonalContract.PersonalEntry._ID + "='" + personal.getCodigo() + "'", null);
        dbWriter.close();

        return rows;
    }

    // listar todos los personal
    public ArrayList<Personal> listarPersonal(){
        dbReader = dataBase.getReadableDatabase();
        String query = ("SELECT * FROM " + PersonalContract.PersonalEntry.TABLE_NAME + ";");
        Cursor c = dbReader.rawQuery(query, null);

        ArrayList<Personal> personals = new ArrayList<Personal>();

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                Personal personal = new Personal();
                personal.setCodigo((int) c.getLong(c.getColumnIndex(PersonalContract.PersonalEntry._ID)));
                personal.setNombres(c.getString(c.getColumnIndex(PersonalContract.PersonalEntry.NOMBRES)));
                personal.setPuesto(c.getString(c.getColumnIndex(PersonalContract.PersonalEntry.PUESTO)));
                personal.setSalario(c.getDouble(c.getColumnIndex(PersonalContract.PersonalEntry.SALARIO)));
                personal.setTelefono(c.getString(c.getColumnIndex(PersonalContract.PersonalEntry.TELEFONO)));
                personal.setEmail(c.getString(c.getColumnIndex(PersonalContract.PersonalEntry.EMAIL)));
                personal.setInfo(c.getString(c.getColumnIndex(PersonalContract.PersonalEntry.INFORMACION)));

                personals.add(personal);
                personal = null;
            } while(c.moveToNext());
        }
        dbReader.close();

        return personals;
    }

    // buscar personal por ID
    public Personal getPersonalByID(int id){
        dbReader = dataBase.getReadableDatabase();
        String query = "SELECT * FROM " + PersonalContract.PersonalEntry.TABLE_NAME + " WHERE " + PersonalContract.PersonalEntry._ID + " = " + id + ";";
        Cursor c = dbReader.rawQuery(query, null);

        if (c != null) { c.moveToFirst(); }

        Personal personal = new Personal();
        personal.setCodigo((int) c.getLong(c.getColumnIndex(PersonalContract.PersonalEntry._ID)));
        personal.setNombres(c.getString(c.getColumnIndex(PersonalContract.PersonalEntry.NOMBRES)));
        personal.setPuesto(c.getString(c.getColumnIndex(PersonalContract.PersonalEntry.PUESTO)));
        personal.setSalario(c.getDouble(c.getColumnIndex(PersonalContract.PersonalEntry.SALARIO)));
        personal.setTelefono(c.getString(c.getColumnIndex(PersonalContract.PersonalEntry.TELEFONO)));
        personal.setEmail(c.getString(c.getColumnIndex(PersonalContract.PersonalEntry.EMAIL)));
        personal.setInfo(c.getString(c.getColumnIndex(PersonalContract.PersonalEntry.INFORMACION)));

        return personal;
    }

    // Eliminar personal
    public long deletePersonal(int id){
        // Eliminar
        dbWriter = dataBase.getWritableDatabase();
        // db.delete("comments", "user=?", args);
        long rows = dbWriter.delete(PersonalContract.PersonalEntry.TABLE_NAME, PersonalContract.PersonalEntry._ID + "='" + id + "'", null);
        dbWriter.close();

        return rows;
    }
}
