package com.appge.appge.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appge.appge.DataBase.CategoriaProdContract;
import com.appge.appge.DataBase.DataBase;
import com.appge.appge.Model.CategoriaProd;

import java.util.ArrayList;

public class CategoriaProdController {

    Context context;
    DataBase dataBase;
    SQLiteDatabase dbWriter;
    SQLiteDatabase dbReader;

    public CategoriaProdController (Context context){
        this.context = context;
        dataBase = new DataBase(context);
        dbWriter = null;
        dbReader = null;
    }

    // Registrar una nueva categoria de producto
    public long addCategoria(CategoriaProd categoria){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(CategoriaProdContract.CategoriaProdEntry.NOMBRE, categoria.getNombre());

        // Insertar
        dbWriter = dataBase.getWritableDatabase();
        long newId = dbWriter.insert(CategoriaProdContract.CategoriaProdEntry.TABLE_NAME, null, values);
        dbWriter.close();

        return newId;
    }

    // Listar todas las categorias
    public ArrayList<CategoriaProd> listarCategoria(){
        dbReader = dataBase.getReadableDatabase();
        String query = ("SELECT * FROM " + CategoriaProdContract.CategoriaProdEntry.TABLE_NAME + ";");
        Cursor c = dbReader.rawQuery(query, null);

        ArrayList<CategoriaProd> categorias = new ArrayList<CategoriaProd>();

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            CategoriaProd categoria;
            do {
                categoria = new CategoriaProd();
                categoria.setCodigo(c.getInt(c.getColumnIndex(CategoriaProdContract.CategoriaProdEntry._ID)));
                categoria.setNombre(c.getString(c.getColumnIndex(CategoriaProdContract.CategoriaProdEntry.NOMBRE)));
                categorias.add(categoria);
                categoria = null;
            } while(c.moveToNext());
        }
        dbReader.close();

        return categorias;
    }

    // buscar Categoria por ID
    public CategoriaProd getCategoriaByID(int id){
        dbReader = dataBase.getReadableDatabase();
        String query = "SELECT * FROM " + CategoriaProdContract.CategoriaProdEntry.TABLE_NAME + " WHERE " + CategoriaProdContract.CategoriaProdEntry._ID + " = " + id + ";";
        Cursor c = dbReader.rawQuery(query, null);

        if (c != null) { c.moveToFirst(); }

        CategoriaProd categoria = new CategoriaProd();
        categoria.setCodigo(c.getInt(c.getColumnIndex(CategoriaProdContract.CategoriaProdEntry._ID)));
        categoria.setNombre(c.getString(c.getColumnIndex(CategoriaProdContract.CategoriaProdEntry.NOMBRE)));

        return categoria;
    }
}
