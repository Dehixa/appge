package com.appge.appge.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appge.appge.DataBase.DataBase;
import com.appge.appge.DataBase.ProductoContract;
import com.appge.appge.Model.Producto;

import java.util.ArrayList;

public class ProductoController {

    Context context;
    DataBase dataBase;
    SQLiteDatabase dbWriter;
    SQLiteDatabase dbReader;

    public ProductoController (Context context){
        this.context = context;
        dataBase = new DataBase(context);
        dbWriter = null;
        dbReader = null;
    }

    // Registrar un nuevo producto
    public long addProducto(Producto producto){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(ProductoContract.ProductoEntry._ID, producto.getCodigo());
        values.put(ProductoContract.ProductoEntry.NOMBRE, producto.getNombre());
        values.put(ProductoContract.ProductoEntry.UNIDAD_COD, producto.getUnidad_cod()); // unidad
        values.put(ProductoContract.ProductoEntry.CATEGORIA_COD, producto.getCategoria_cod()); // categoria
        values.put(ProductoContract.ProductoEntry.CANTIDAD, producto.getCantidad());
        values.put(ProductoContract.ProductoEntry.CANTIDAD_RESERVA, producto.getCantidad_reserv());
        values.put(ProductoContract.ProductoEntry.PRECIO_COMPRA, producto.getPrecio_compra());
        values.put(ProductoContract.ProductoEntry.PRECIO_VENTA, producto.getPrecio_venta());
        values.put(ProductoContract.ProductoEntry.FECHA, producto.getFecha());
        values.put(ProductoContract.ProductoEntry.INFORMACION, producto.getInfo());
        values.put(ProductoContract.ProductoEntry.IMAGEN, producto.getImage());

        // Insertar
        dbWriter = dataBase.getWritableDatabase();
        long newId = dbWriter.insert(ProductoContract.ProductoEntry.TABLE_NAME, null, values);
        dbWriter.close();

        return newId;
    }

    // Modificar un producto
    public long editProducto(Producto producto){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(ProductoContract.ProductoEntry.NOMBRE, producto.getNombre());
        values.put(ProductoContract.ProductoEntry.UNIDAD_COD, producto.getUnidad_cod()); // unidad
        values.put(ProductoContract.ProductoEntry.CATEGORIA_COD, producto.getCategoria_cod()); // categoria
        values.put(ProductoContract.ProductoEntry.CANTIDAD, producto.getCantidad());
        values.put(ProductoContract.ProductoEntry.CANTIDAD_RESERVA, producto.getCantidad_reserv());
        values.put(ProductoContract.ProductoEntry.PRECIO_COMPRA, producto.getPrecio_compra());
        values.put(ProductoContract.ProductoEntry.PRECIO_VENTA, producto.getPrecio_venta());
        values.put(ProductoContract.ProductoEntry.FECHA, producto.getFecha());
        values.put(ProductoContract.ProductoEntry.INFORMACION, producto.getInfo());
        values.put(ProductoContract.ProductoEntry.IMAGEN, producto.getImage());

        // Modificar
        dbWriter = dataBase.getWritableDatabase();
        long rows = dbWriter.update(ProductoContract.ProductoEntry.TABLE_NAME, values, ProductoContract.ProductoEntry._ID + "='" + producto.getCodigo() + "'", null);
        dbWriter.close();

        return rows;
    }

    // Modificar cantidad de un producto
    public long editCantidad(int id, int cantidad){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(ProductoContract.ProductoEntry.CANTIDAD, cantidad);

        // Modificar
        dbWriter = dataBase.getWritableDatabase();
        long rows = dbWriter.update(ProductoContract.ProductoEntry.TABLE_NAME, values, ProductoContract.ProductoEntry._ID + "='" + id + "'", null);
        dbWriter.close();

        return rows;
    }

    // listar todos los productos
    public ArrayList<Producto> listarProductos(){
        dbReader = dataBase.getReadableDatabase();
        String query = ("SELECT * FROM " + ProductoContract.ProductoEntry.TABLE_NAME + ";");
        Cursor c = dbReader.rawQuery(query, null);

        ArrayList<Producto> productos = new ArrayList<Producto>();

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                Producto producto = new Producto();
                producto.setCodigo((int)c.getLong(c.getColumnIndex(ProductoContract.ProductoEntry._ID)));
                producto.setNombre(c.getString(c.getColumnIndex(ProductoContract.ProductoEntry.NOMBRE)));
                producto.setUnidad_cod(c.getInt(c.getColumnIndex(ProductoContract.ProductoEntry.UNIDAD_COD)));
                producto.setCategoria_cod(c.getInt(c.getColumnIndex(ProductoContract.ProductoEntry.CATEGORIA_COD)));
                producto.setCantidad(c.getInt(c.getColumnIndex(ProductoContract.ProductoEntry.CANTIDAD)));
                producto.setCantidad_reserv(c.getInt(c.getColumnIndex(ProductoContract.ProductoEntry.CANTIDAD_RESERVA)));
                producto.setPrecio_compra(c.getDouble(c.getColumnIndex(ProductoContract.ProductoEntry.PRECIO_COMPRA)));
                producto.setPrecio_venta(c.getDouble(c.getColumnIndex(ProductoContract.ProductoEntry.PRECIO_VENTA)));
                producto.setFecha(c.getString(c.getColumnIndex(ProductoContract.ProductoEntry.FECHA)));
                producto.setInfo(c.getString(c.getColumnIndex(ProductoContract.ProductoEntry.INFORMACION)));
                producto.setImage(c.getBlob(c.getColumnIndex(ProductoContract.ProductoEntry.IMAGEN)));
                productos.add(producto);
                producto = null;
            } while(c.moveToNext());
        }
        dbReader.close();

        return productos;
    }

    // Obtener codigo del siguiente producto a registrar
    public int getNewCodigo(){
        dbReader = dataBase.getReadableDatabase();
        String query = ("SELECT " + ProductoContract.ProductoEntry._ID + " FROM " + ProductoContract.ProductoEntry.TABLE_NAME + ";");
        Cursor c = dbReader.rawQuery(query, null);

        int codigo = 0;

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                codigo = (int) c.getLong(c.getColumnIndex(ProductoContract.ProductoEntry._ID));
            } while(c.moveToNext());
        }
        dbReader.close();

        return codigo + 1;
    }

    // buscar Producto por ID
    public Producto getProductoByID(int id){
        dbReader = dataBase.getReadableDatabase();
        String query = "SELECT * FROM " + ProductoContract.ProductoEntry.TABLE_NAME + " WHERE " + ProductoContract.ProductoEntry._ID + " = " + id + ";";
        Cursor c = dbReader.rawQuery(query, null);

        if (c != null) { c.moveToFirst(); }

        Producto producto = new Producto();
        producto.setCodigo((int)c.getLong(c.getColumnIndex(ProductoContract.ProductoEntry._ID)));
        producto.setNombre(c.getString(c.getColumnIndex(ProductoContract.ProductoEntry.NOMBRE)));
        producto.setUnidad_cod(c.getInt(c.getColumnIndex(ProductoContract.ProductoEntry.UNIDAD_COD)));
        producto.setCategoria_cod(c.getInt(c.getColumnIndex(ProductoContract.ProductoEntry.CATEGORIA_COD)));
        producto.setCantidad(c.getInt(c.getColumnIndex(ProductoContract.ProductoEntry.CANTIDAD)));
        producto.setCantidad_reserv(c.getInt(c.getColumnIndex(ProductoContract.ProductoEntry.CANTIDAD_RESERVA)));
        producto.setPrecio_compra(c.getDouble(c.getColumnIndex(ProductoContract.ProductoEntry.PRECIO_COMPRA)));
        producto.setPrecio_venta(c.getDouble(c.getColumnIndex(ProductoContract.ProductoEntry.PRECIO_VENTA)));
        producto.setFecha(c.getString(c.getColumnIndex(ProductoContract.ProductoEntry.FECHA)));
        producto.setInfo(c.getString(c.getColumnIndex(ProductoContract.ProductoEntry.INFORMACION)));
        producto.setImage(c.getBlob(c.getColumnIndex(ProductoContract.ProductoEntry.IMAGEN)));

        return producto;
    }

    // Eliminar un producto
    public long deleteProducto(int id){
        // Eliminar
        dbWriter = dataBase.getWritableDatabase();
        // db.delete("comments", "user=?", args);
        long rows = dbWriter.delete(ProductoContract.ProductoEntry.TABLE_NAME, ProductoContract.ProductoEntry._ID + "='" + id + "'", null);
        dbWriter.close();

        return rows;
    }

}
