package com.appge.appge.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appge.appge.DataBase.DataBase;
import com.appge.appge.DataBase.VentaDetContract;
import com.appge.appge.Model.VentaDetalle;

import java.util.ArrayList;

public class VentaDetController {

    Context context;
    DataBase dataBase;
    SQLiteDatabase dbWriter;
    SQLiteDatabase dbReader;

    public VentaDetController (Context context){
        this.context = context;
        dataBase = new DataBase(context);
        dbWriter = null;
        dbReader = null;
    }

    // Registrar una venta detalle
    public long addVentaDet(VentaDetalle venta){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(VentaDetContract.VentaDetEntry.VENTA_CAB_ID, venta.getCodigoVentaCab());
        values.put(VentaDetContract.VentaDetEntry.PRODUCTO_ID, venta.getCodigoProducto());
        values.put(VentaDetContract.VentaDetEntry.CANTIDAD, venta.getCantidad());
        values.put(VentaDetContract.VentaDetEntry.SUBTOTAL, venta.getSubtotal());

        // Insertar
        dbWriter = dataBase.getWritableDatabase();
        long newId = dbWriter.insert(VentaDetContract.VentaDetEntry.TABLE_NAME, null, values);
        dbWriter.close();

        return newId;
    }

    // buscar ventas detalles por ID de venta cabecera
    public ArrayList<VentaDetalle> getVentaDetByIDVenta(int idVenta){
        dbReader = dataBase.getReadableDatabase();
        String query = "SELECT * FROM " + VentaDetContract.VentaDetEntry.TABLE_NAME + " WHERE " + VentaDetContract.VentaDetEntry.VENTA_CAB_ID + " = " + idVenta + ";";
        Cursor c = dbReader.rawQuery(query, null);

        ArrayList<VentaDetalle> ventas = new ArrayList<VentaDetalle>();

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                VentaDetalle venta = new VentaDetalle();
                venta.setCodigoVentaCab((int)c.getLong(c.getColumnIndex(VentaDetContract.VentaDetEntry.VENTA_CAB_ID)));
                venta.setCodigoProducto((int)c.getLong(c.getColumnIndex(VentaDetContract.VentaDetEntry.PRODUCTO_ID)));
                venta.setCantidad(c.getInt(c.getColumnIndex(VentaDetContract.VentaDetEntry.CANTIDAD)));
                venta.setSubtotal(c.getDouble(c.getColumnIndex(VentaDetContract.VentaDetEntry.SUBTOTAL)));
                ventas.add(venta);
                venta = null;
            } while(c.moveToNext());
        }
        dbReader.close();

        return ventas;
    }
}
