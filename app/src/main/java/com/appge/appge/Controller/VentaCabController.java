package com.appge.appge.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appge.appge.DataBase.DataBase;
import com.appge.appge.DataBase.VentaCabContract;
import com.appge.appge.Model.VentaCabecera;

import java.util.ArrayList;

public class VentaCabController {

    Context context;
    DataBase dataBase;
    SQLiteDatabase dbWriter;
    SQLiteDatabase dbReader;

    public VentaCabController (Context context){
        this.context = context;
        dataBase = new DataBase(context);
        dbWriter = null;
        dbReader = null;
    }

    // Registrar una venta
    public long addVenta(VentaCabecera venta){
        // Contenedor de valores
        ContentValues values = new ContentValues();

        // Pares clave-valor
        values.put(VentaCabContract.VentaCabEntry._ID, venta.getCodigo());
        values.put(VentaCabContract.VentaCabEntry.FECHA, venta.getFecha());
        values.put(VentaCabContract.VentaCabEntry.CLIENTE, venta.getCliente());
        values.put(VentaCabContract.VentaCabEntry.FORMA_PAGO, venta.getFormaPago());
        values.put(VentaCabContract.VentaCabEntry.TOTAL, venta.getTotal());

        // Insertar
        dbWriter = dataBase.getWritableDatabase();
        long newId = dbWriter.insert(VentaCabContract.VentaCabEntry.TABLE_NAME, null, values);
        dbWriter.close();

        return newId;
    }

    // listar ventas
    public ArrayList<VentaCabecera> listarVentas(){
        dbReader = dataBase.getReadableDatabase();
        String query = ("SELECT * FROM " + VentaCabContract.VentaCabEntry.TABLE_NAME + ";");
        Cursor c = dbReader.rawQuery(query, null);

        ArrayList<VentaCabecera> ventas = new ArrayList<VentaCabecera>();

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                VentaCabecera venta = new VentaCabecera();
                venta.setCodigo((int)c.getLong(c.getColumnIndex(VentaCabContract.VentaCabEntry._ID)));
                venta.setFecha(c.getString(c.getColumnIndex(VentaCabContract.VentaCabEntry.FECHA)));
                venta.setCliente(c.getString(c.getColumnIndex(VentaCabContract.VentaCabEntry.CLIENTE)));
                venta.setFormaPago(c.getString(c.getColumnIndex(VentaCabContract.VentaCabEntry.FORMA_PAGO)));
                venta.setTotal(c.getDouble(c.getColumnIndex(VentaCabContract.VentaCabEntry.TOTAL)));
                ventas.add(venta);
                venta = null;
            } while(c.moveToNext());
        }
        dbReader.close();

        return ventas;
    }

    // Obtener codigo del siguiente producto a registrar
    public int getNewCodigo(){
        dbReader = dataBase.getReadableDatabase();
        String query = ("SELECT " + VentaCabContract.VentaCabEntry._ID + " FROM " + VentaCabContract.VentaCabEntry.TABLE_NAME + ";");
        Cursor c = dbReader.rawQuery(query, null);

        int codigo = 0;

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                codigo = (int) c.getLong(c.getColumnIndex(VentaCabContract.VentaCabEntry._ID));
            } while(c.moveToNext());
        }
        dbReader.close();

        return codigo + 1;
    }

    // buscar Ventas por Fecha
    public ArrayList<VentaCabecera> getVentaByFecha(String fecha){
        dbReader = dataBase.getReadableDatabase();
        String query = "SELECT * FROM " + VentaCabContract.VentaCabEntry.TABLE_NAME + " WHERE " + VentaCabContract.VentaCabEntry.FECHA + " = '" + fecha + "';";
        Cursor c = dbReader.rawQuery(query, null);

        ArrayList<VentaCabecera> ventas = new ArrayList<VentaCabecera>();

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                VentaCabecera venta = new VentaCabecera();
                venta.setCodigo((int)c.getLong(c.getColumnIndex(VentaCabContract.VentaCabEntry._ID)));
                venta.setFecha(c.getString(c.getColumnIndex(VentaCabContract.VentaCabEntry.FECHA)));
                venta.setCliente(c.getString(c.getColumnIndex(VentaCabContract.VentaCabEntry.CLIENTE)));
                venta.setFormaPago(c.getString(c.getColumnIndex(VentaCabContract.VentaCabEntry.FORMA_PAGO)));
                venta.setTotal(c.getDouble(c.getColumnIndex(VentaCabContract.VentaCabEntry.TOTAL)));
                ventas.add(venta);
                venta = null;
            } while(c.moveToNext());
        }
        dbReader.close();

        return ventas;
    }

    // listar ventas
    public ArrayList<VentaCabecera> getVentas(){
        dbReader = dataBase.getReadableDatabase();
        String query = "SELECT * FROM " + VentaCabContract.VentaCabEntry.TABLE_NAME;
        Cursor c = dbReader.rawQuery(query, null);

        ArrayList<VentaCabecera> ventas = new ArrayList<VentaCabecera>();

        //Nos aseguramos de que existe al menos un registro
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no haya más registros
            do {
                VentaCabecera venta = new VentaCabecera();
                venta.setCodigo((int)c.getLong(c.getColumnIndex(VentaCabContract.VentaCabEntry._ID)));
                venta.setFecha(c.getString(c.getColumnIndex(VentaCabContract.VentaCabEntry.FECHA)));
                venta.setCliente(c.getString(c.getColumnIndex(VentaCabContract.VentaCabEntry.CLIENTE)));
                venta.setFormaPago(c.getString(c.getColumnIndex(VentaCabContract.VentaCabEntry.FORMA_PAGO)));
                venta.setTotal(c.getDouble(c.getColumnIndex(VentaCabContract.VentaCabEntry.TOTAL)));
                ventas.add(venta);
                venta = null;
            } while(c.moveToNext());
        }
        dbReader.close();

        return ventas;
    }
}
