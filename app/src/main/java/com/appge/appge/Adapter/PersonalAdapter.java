package com.appge.appge.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appge.appge.Model.Personal;
import com.appge.appge.R;

import java.util.List;

public class PersonalAdapter extends ArrayAdapter<Personal> {


    private List<Personal> items;
    Context context;

    public PersonalAdapter(Context context, int textViewResourceId, List<Personal> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.items = objects;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Personal getItem(int index) {
        return items.get(index);
    }

    @Override
    public long getItemId(int index) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.row_personal, parent, false);

        }

        Personal item = getItem(index);

        // Referencias UI.
        TextView nombre = view.findViewById(R.id.text_nombre_pers);
        TextView puesto = view.findViewById(R.id.text_puesto_pers);
        TextView info = view.findViewById(R.id.text_info_pers);
        TextView telefono = view.findViewById(R.id.text_telefono_pers);
        TextView correo = view.findViewById(R.id.text_correo_pers);

        // Setup.
        nombre.setText(item.getNombres());
        puesto.setText(item.getPuesto());
        telefono.setText(item.getTelefono());
        correo.setText(item.getEmail());

        if (item.getInfo().equals("") || item.getInfo() == null){
            info.setText("Sin Información");
        }else{
            info.setText(item.getInfo());
        }


        return view;

    }

}

