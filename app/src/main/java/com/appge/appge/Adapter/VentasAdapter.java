package com.appge.appge.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.view.View.OnFocusChangeListener;

import com.appge.appge.Listener.VentasListener;
import com.appge.appge.Model.Producto;
import com.appge.appge.R;
import com.appge.appge.VentasFragment;

import java.util.List;

public class VentasAdapter extends ArrayAdapter<Producto> {


    private List<Producto> items;
    Context context;
    private VentasListener listener;
    private Producto item;

    public VentasAdapter(Context context, int textViewResourceId, List<Producto> objects, VentasListener listener) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.items = objects;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Producto getItem(int index) {
        return items.get(index);
    }

    @Override
    public long getItemId(int index) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.row_prod_ventas, parent, false);

        }

        item = getItem(index);

        final int cant_total = item.getCantidad();

        // Referencias UI.
        final TextView producto = view.findViewById(R.id.text_producto_venta);
        final TextInputEditText cantidad = view.findViewById(R.id.editText_cantidad_venta);
        final TextInputEditText precio = view.findViewById(R.id.editText_precio_venta);
        final TextInputEditText subtotal = view.findViewById(R.id.editText_subtotal);

        precio.setEnabled(false);
        subtotal.setEnabled(false);

        // Setup.
        producto.setText(item.getCodigo() + " - " +item.getNombre());
        precio.setText(String.valueOf(item.getPrecio_venta()));

        //we need to update adapter once we finish with editing
        cantidad.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
                    TextInputEditText text = (TextInputEditText) v;

                    if (!text.getText().toString().equals("")){
                        String codP = producto.getText().toString();
                        int cod = Integer.valueOf(codP.substring(0, codP.indexOf(" ")));
                        int cant = Integer.valueOf(text.getText().toString());
                        if (cant < cant_total){
                            double sub = cant * Double.valueOf(precio.getText().toString());
                            subtotal.setText(String.valueOf(sub));
                            VentasFragment.PRECIO_TOTAL.put(cod, sub);
                            VentasFragment.CANTIDADES.put(cod, cant);
                            listener.onReturnPrice();
                        } else {
                            subtotal.setText("0");
                        }
                    }
                }
            }
        });

        return view;

    }

}

