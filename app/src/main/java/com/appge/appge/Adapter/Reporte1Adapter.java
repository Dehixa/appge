package com.appge.appge.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appge.appge.Controller.ProductoController;
import com.appge.appge.Model.Producto;
import com.appge.appge.Model.VentaDetalle;
import com.appge.appge.R;

import java.util.List;

public class Reporte1Adapter extends ArrayAdapter<VentaDetalle> {


    private List<VentaDetalle> items;
    Context context;

    public Reporte1Adapter(Context context, int textViewResourceId, List<VentaDetalle> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.items = objects;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public VentaDetalle getItem(int index) {
        return items.get(index);
    }

    @Override
    public long getItemId(int index) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.row_prod_reporte1, parent, false);

        }

        VentaDetalle item = getItem(index);

        // Referencias UI.
        TextView codigo = view.findViewById(R.id.text_id_prod_report1);
        TextView producto = view.findViewById(R.id.text_prod_report1);
        TextView cantidad = view.findViewById(R.id.text_cant_report1);
        TextView ganancia = view.findViewById(R.id.text_ganancia_report1);

        // Setup.
        ProductoController productoController = new ProductoController(getContext());
        Producto prod = productoController.getProductoByID(item.getCodigoProducto());
        // ganancia = precio_total_venta - precio_total_compra
        double gana = (item.getCantidad() * prod.getPrecio_venta()) - (item.getCantidad() * prod.getPrecio_compra());

        codigo.setText(String.valueOf(String.valueOf(item.getCodigoProducto())));
        producto.setText(prod.getNombre());
        cantidad.setText(String.valueOf(item.getCantidad()));
        ganancia.setText("S/. " + String.valueOf(gana));

        return view;

    }

}