package com.appge.appge.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appge.appge.Model.Producto;
import com.appge.appge.R;

import java.util.List;

public class ProductoAdapter extends ArrayAdapter<Producto> {


    private List<Producto> items;
    Context context;

    public ProductoAdapter(Context context, int textViewResourceId, List<Producto> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.items = objects;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Producto getItem(int index) {
        return items.get(index);
    }

    @Override
    public long getItemId(int index) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.row_productos, parent, false);

        }

        Producto item = getItem(index);

        // Referencias UI.
        TextView codigo = view.findViewById(R.id.text_codigo_prod);
        TextView nombre = view.findViewById(R.id.text_nombre_prod);
        TextView cantidad = view.findViewById(R.id.text_cantidad_prod);
        TextView venta = view.findViewById(R.id.text_venta_prod);

        // Setup.
        codigo.setText(String.valueOf(item.getCodigo()));
        nombre.setText(item.getNombre());
        cantidad.setText(String.valueOf(item.getCantidad()));
        venta.setText(String.valueOf(item.getPrecio_venta()));

        return view;

    }

}
