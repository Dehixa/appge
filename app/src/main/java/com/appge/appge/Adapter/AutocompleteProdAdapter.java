package com.appge.appge.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Filter;

import com.appge.appge.Model.Personal;
import com.appge.appge.Model.Producto;
import com.appge.appge.R;

import java.util.ArrayList;
import java.util.List;

public class AutocompleteProdAdapter extends ArrayAdapter<Producto> {


    private List<Producto> items;
    private ItemFilter itemFilter = new ItemFilter();
    private List<Producto> itemBase = null;
    Context context;

    public AutocompleteProdAdapter(Context context, int textViewResourceId, List<Producto> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.itemBase = objects;
        this.items = objects;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Producto getItem(int index) {
        return items.get(index);
    }

    @Override
    public long getItemId(int index) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.row_prod_autocomplete, parent, false);

        }

        Producto item = getItem(index);

        // Referencias UI.
        TextView codigo = view.findViewById(R.id.text_cod_prod_auto);
        TextView nombre = view.findViewById(R.id.text_nomb_prod_auto);

        // Setup.
        codigo.setText(String.valueOf(item.getCodigo()));
        nombre.setText(item.getNombre());

        return view;

    }

    public Filter getFilter() {
        return itemFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            final List<Producto> list = itemBase;

            int count = list.size();
            final ArrayList<Producto> nlist = new ArrayList<Producto>(count);

            Producto item;

            for (int i = 0; i < count; i++) {
                item = list.get(i);
                if (item.getNombre().toLowerCase().contains(filterString)) {
                    nlist.add(item);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.values != null) {
                items = (ArrayList<Producto>)results.values;
            } else {
                items = null;
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

    }
}