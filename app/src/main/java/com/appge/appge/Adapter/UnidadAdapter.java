package com.appge.appge.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appge.appge.Model.Unidad;
import com.appge.appge.R;

import java.util.List;

public class UnidadAdapter extends ArrayAdapter<Unidad> {


    private List<Unidad> items;
    Context ctxt;

    public UnidadAdapter(Context context, int textViewResourceId, List<Unidad> objects) {
        super(context, textViewResourceId, objects);
        ctxt = context;
        this.items = objects;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Unidad getItem(int index) {
        return items.get(index);
    }

    @Override
    public long getItemId(int index) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.row_elemento, parent, false);

        }

        Unidad item = getItem(index);

        // Referencias UI.
        TextView textNombre = (TextView) view.findViewById(R.id.text_nombre_elemento);

        // Setup.
        textNombre.setText(item.getNombre());
        textNombre.setCompoundDrawablesWithIntrinsicBounds( R.drawable.ic_unidad_black,0, 0, 0);

        return view;

    }

}
