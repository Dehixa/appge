package com.appge.appge.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appge.appge.Model.Cliente;
import com.appge.appge.R;

import java.util.List;

public class ClienteAdapter extends ArrayAdapter<Cliente> {


    private List<Cliente> items;
    Context context;

    public ClienteAdapter(Context context, int textViewResourceId, List<Cliente> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.items = objects;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Cliente getItem(int index) {
        return items.get(index);
    }

    @Override
    public long getItemId(int index) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.row_clientes, parent, false);

        }

        Cliente item = getItem(index);

        // Referencias UI.
        TextView nombre = view.findViewById(R.id.text_nombre_cli);
        TextView telefono = view.findViewById(R.id.text_telefono_cli);
        TextView correo = view.findViewById(R.id.text_correo_cli);
        TextView direccion = view.findViewById(R.id.text_direccion_cli);

        // Setup.
        nombre.setText(item.getNombres());
        telefono.setText(item.getTelefono());
        correo.setText(item.getEmail());
        direccion.setText(item.getDireccion());

        return view;

    }

}
