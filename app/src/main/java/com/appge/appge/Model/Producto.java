package com.appge.appge.Model;

public class Producto {

    private int codigo;
    private String nombre;
    private int unidad_cod;
    private int categoria_cod;
    private int cantidad;
    private int cantidad_reserv;
    private double precio_compra;
    private double precio_venta;
    private String fecha;
    private String info;
    private byte[] image;

    public Producto(){}

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getUnidad_cod() {
        return unidad_cod;
    }

    public void setUnidad_cod(int unidad_cod) {
        this.unidad_cod = unidad_cod;
    }

    public int getCategoria_cod() {
        return categoria_cod;
    }

    public void setCategoria_cod(int categoria_cod) {
        this.categoria_cod = categoria_cod;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCantidad_reserv() {
        return cantidad_reserv;
    }

    public void setCantidad_reserv(int cantidad_reserv) {
        this.cantidad_reserv = cantidad_reserv;
    }

    public double getPrecio_compra() {
        return precio_compra;
    }

    public void setPrecio_compra(double precio_compra) {
        this.precio_compra = precio_compra;
    }

    public double getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(double precio_venta) {
        this.precio_venta = precio_venta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
