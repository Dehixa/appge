package com.appge.appge.Model;

public class VentaDetalle {

    private int codigoVentaCab;
    private int codigoProducto;
    private int cantidad;
    private double subtotal;

    public int getCodigoVentaCab() {
        return codigoVentaCab;
    }

    public void setCodigoVentaCab(int codigoVentaCab) {
        this.codigoVentaCab = codigoVentaCab;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }
}
