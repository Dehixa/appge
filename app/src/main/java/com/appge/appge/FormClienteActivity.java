package com.appge.appge;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.appge.appge.Controller.ClienteController;
import com.appge.appge.Model.Cliente;

public class FormClienteActivity extends AppCompatActivity {

    TextInputEditText nombres;
    TextInputEditText telefono;
    TextInputEditText email;
    TextInputEditText direccion;
    TextInputEditText info;

    TextInputLayout layout_nombres;
    TextInputLayout layout_telefono;
    TextInputLayout layout_email;
    TextInputLayout layout_direccion;
    TextInputLayout layout_info;

    private int clienteID;
    private String opcion;
    private ClienteController controller;
    private Menu menu;
    private MenuItem item_edit;
    private MenuItem item_delete;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_cliente);
        toolbar = (Toolbar) findViewById(R.id.toolbar5);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        controller = new ClienteController(this);
        clienteID = 0;
        opcion = "ver";

        nombres =  findViewById(R.id.editText_nombres_cli2);
        telefono = findViewById(R.id.editText_telefono_cli2);
        email = findViewById(R.id.editText_email_cli2);
        direccion = findViewById(R.id.editText_direccion_cli2);
        info = findViewById(R.id.editText_info_cli2);

        // Guardamos TextLayout (Caja donde esta el EditText)
        layout_nombres = findViewById(R.id.text_input_nombres_cli2);
        layout_telefono = findViewById(R.id.text_input_telefono_cli2);
        layout_email = findViewById(R.id.text_input_email_cli2);
        layout_direccion = findViewById(R.id.text_input_direccion_cli2);
        layout_info = findViewById(R.id.text_input_info_cli2);

        hideKeyboard();

        toolbar.setTitle("Detalle del Cliente");
        clienteID = getIntent().getExtras().getInt("clienteID");
        cargarCliente();

        deshabilitarCampos();
    }

    @Override public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_form_editar2, this.menu);

        item_edit = menu.getItem(0);
        item_delete = menu.getItem(1);

        return super.onCreateOptionsMenu(this.menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_2:
                if (opcion.equals("ver")){
                    opcion = "editar";
                    habilitarCampos();
                    toolbar.setTitle("Editar Cliente");
                    item_edit.setIcon(getResources().getDrawable(R.drawable.ic_check_white));
                } else if (opcion.equals("editar")){
                    opcion = "ver";
                    toolbar.setTitle("Detalle del Cliente");
                    deshabilitarCampos();
                    item_edit.setIcon(getResources().getDrawable(R.drawable.ic_edit_white));

                    if (validar()) {
                        // Se edita en la base de datos
                        Cliente cliente = new Cliente();
                        cliente.setCodigo(clienteID);
                        cliente.setNombres(nombres.getText().toString());
                        cliente.setTelefono(telefono.getText().toString());
                        cliente.setEmail(email.getText().toString());
                        cliente.setDireccion(direccion.getText().toString());
                        cliente.setInfo(info.getText().toString());

                        long rows = controller.editCliente(cliente);

                        if (rows > 0)
                            Toast.makeText(this, "Cliente modificado exitosamente.", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(this, "Error al modificar.", Toast.LENGTH_SHORT).show();
                    }
                }

                return true;

            case R.id.action_delete_2:
                if (opcion.equals("ver")){
                    // borrar registro
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
                    String mensaje = "¿Desea eliminar este cliente?";
                    dlgAlert.setMessage(mensaje);
                    dlgAlert.setTitle("Eliminar");
                    dlgAlert.setPositiveButton("Eliminar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    eliminarCliente();
                                    onBackPressed();
                                }
                            });
                    dlgAlert.setNegativeButton("Cancelar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    dlgAlert.create().show();

                } else if (opcion.equals("editar")){
                    Toast.makeText(this, "Debe guardar primero las modificaciones realizadas.", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void hideKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void cargarCliente(){
        Cliente cliente = controller.getClienteByID(clienteID);
        nombres.setText(cliente.getNombres());
        telefono.setText(cliente.getTelefono());
        email.setText(cliente.getEmail());
        direccion.setText(cliente.getDireccion());
        info.setText(cliente.getInfo());

        nombres.requestFocus();
    }

    private void eliminarCliente(){
        long rowDelete = controller.deleteCliente(clienteID);
        if (rowDelete > 0)
            Toast.makeText(this, "Cliente eliminado.", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Error al eliminar.", Toast.LENGTH_SHORT).show();
    }

    private void habilitarCampos(){
        nombres.setEnabled(true);
        telefono.setEnabled(true);
        email.setEnabled(true);
        direccion.setEnabled(true);
        info.setEnabled(true);

        nombres.requestFocus();
    }

    private void deshabilitarCampos(){
        nombres.setEnabled(false);
        telefono.setEnabled(false);
        email.setEnabled(false);
        direccion.setEnabled(false);
        info.setEnabled(false);

        nombres.requestFocus();
    }

    private boolean validar() {
        boolean valido = true;

        // Comprueba que el campo de Nombres y Apellidos no este vacio.
        if (TextUtils.isEmpty(nombres.getText())) {
            layout_nombres.setError("Debe ingresar Nombres y Apellidos.");
            layout_nombres.setErrorEnabled(true);
            valido = false;
        }else{
            layout_nombres.setErrorEnabled(false);
        }

        // Comprueba que el campo de Telefono no este vacio.
        if (TextUtils.isEmpty(telefono.getText())) {
            layout_telefono.setError("Debe ingresar un Número telefónico.");
            layout_telefono.setErrorEnabled(true);
            valido = false;
        }else{
            layout_telefono.setErrorEnabled(false);
        }
        // Comprueba que el campo de Email no este vacio o que contenga un @.
        if (TextUtils.isEmpty(email.getText())) {
            layout_email.setError("Debe ingresar un Email.");
            layout_email.setErrorEnabled(true);
            valido = false;
        }else if (TextUtils.indexOf(email.getText(), "@") == -1){
            layout_email.setError("El email debe contener un @.");
            layout_email.setErrorEnabled(true);
            valido = false;
        }else{
            layout_email.setErrorEnabled(false);
        }

        // Comprueba que el campo de Salario no este vacio.
        if (TextUtils.isEmpty(direccion.getText())) {
            layout_direccion.setError("Debe ingresar una Dirección.");
            layout_direccion.setErrorEnabled(true);
            valido = false;
        }else{
            layout_direccion.setErrorEnabled(false);
        }

        return valido;
    }

}
