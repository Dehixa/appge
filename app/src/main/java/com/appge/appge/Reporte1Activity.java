package com.appge.appge;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ListView;

import com.appge.appge.Adapter.Reporte1Adapter;
import com.appge.appge.Controller.ProductoController;
import com.appge.appge.Controller.VentaCabController;
import com.appge.appge.Controller.VentaDetController;
import com.appge.appge.Dialog.DatePickerFragment;
import com.appge.appge.Model.Producto;
import com.appge.appge.Model.VentaCabecera;
import com.appge.appge.Model.VentaDetalle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class Reporte1Activity extends AppCompatActivity {

    private TextInputLayout layout_fecha;
    private TextInputEditText fecha;
    private ListView lista;

    private ArrayList<VentaCabecera> cabeceras;
    private ArrayList<VentaDetalle> detalles;

    private VentaCabController ventaCabController;
    private VentaDetController ventaDetController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar6);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ventaCabController = new VentaCabController(this);
        ventaDetController = new VentaDetController(this);

        detalles = new ArrayList<>();

        layout_fecha = findViewById(R.id.text_input_fecha_reporte1);
        fecha = findViewById(R.id.editText_fecha_reporte1);
        lista = findViewById(R.id.listview_prod_reporte1);

        fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because January is zero
                final String selectedDate = day + " / " + (month+1) + " / " + year;
                fecha.setText(selectedDate);

                // actualizar lista
                cabeceras = ventaCabController.getVentaByFecha(selectedDate);
                if (!cabeceras.isEmpty()){
                    getVentaDetalle();
                    Reporte1Adapter adapter = new Reporte1Adapter(getApplicationContext(), R.layout.row_prod_reporte1, detalles);
                    lista.setAdapter(adapter);
                } else {
                    detalles.clear();
                    Reporte1Adapter adapter = new Reporte1Adapter(getApplicationContext(), R.layout.row_prod_reporte1, detalles);
                    lista.setAdapter(adapter);
                }
            }
        });

        newFragment.show(this.getSupportFragmentManager(), "datePicker");
    }

    private void getVentaDetalle(){
        HashMap<Integer, Integer> cantidades = new HashMap<>();

        for (int i = 0; i < cabeceras.size(); i++){
            ArrayList<VentaDetalle> ventaDetalles = ventaDetController.getVentaDetByIDVenta(cabeceras.get(i).getCodigo());

            for (int j = 0; j < ventaDetalles.size(); j++){
                if (cantidades.containsKey(ventaDetalles.get(j).getCodigoProducto())){
                    int aux = cantidades.get(ventaDetalles.get(j).getCodigoProducto()) + ventaDetalles.get(j).getCantidad();
                    cantidades.put(ventaDetalles.get(j).getCodigoProducto(), aux);
                } else {
                    cantidades.put(ventaDetalles.get(j).getCodigoProducto(), ventaDetalles.get(j).getCantidad());
                }

            }
        }

        for (HashMap.Entry<Integer,Integer> entry : cantidades.entrySet()) {
            VentaDetalle ventaDetalle = new VentaDetalle();
            ventaDetalle.setCodigoProducto(entry.getKey());
            ventaDetalle.setCantidad(entry.getValue());
            detalles.add(ventaDetalle);
        }
    }

}
