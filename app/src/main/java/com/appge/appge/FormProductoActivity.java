package com.appge.appge;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.Toast;

import com.appge.appge.Controller.CategoriaProdController;
import com.appge.appge.Controller.ProductoController;
import com.appge.appge.Controller.UnidadController;
import com.appge.appge.Dialog.DatePickerFragment;
import com.appge.appge.Dialog.ElementoDialog;
import com.appge.appge.Dialog.ListaCategoriaProdDialog;
import com.appge.appge.Dialog.ListaUnidadDialog;
import com.appge.appge.Listener.DialogCategoriaProdListener;
import com.appge.appge.Listener.DialogUnidadListener;
import com.appge.appge.Model.CategoriaProd;
import com.appge.appge.Model.Producto;
import com.appge.appge.Model.Unidad;

import org.json.JSONException;
import org.json.JSONObject;

public class FormProductoActivity extends AppCompatActivity implements ElementoDialog.DialogListener, DialogUnidadListener,
        DialogCategoriaProdListener {

    private String opcion;
    private Menu menu;
    private MenuItem item_edit;
    private MenuItem item_add_delete;
    private TextInputLayout layout_codigo;
    private TextInputLayout layout_nombre;
    private TextInputLayout layout_unidad;
    private TextInputLayout layout_categoria;
    private TextInputLayout layout_cantidad;
    private TextInputLayout layout_cantidad_reserva;
    private TextInputLayout layout_precio_compra;
    private TextInputLayout layout_precio_venta;
    private TextInputLayout layout_fecha_venc;
    private TextInputLayout layout_info;

    private TextInputEditText codigo;
    private TextInputEditText nombre;
    private TextInputEditText unidad;
    private TextInputEditText categoria;
    private TextInputEditText cantidad;
    private TextInputEditText cantidad_reserva;
    private TextInputEditText precio_compra;
    private TextInputEditText precio_venta;
    private TextInputEditText fecha_venc;
    private TextInputEditText info;

    private Toolbar toolbar;
    private int idUnidad;
    private int idCategoria;

    private ProductoController controller;
    private int productoID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_producto);
        toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        controller = new ProductoController(this);

        productoID = 0;
        opcion = getIntent().getExtras().getString("opcion");

        layout_codigo = findViewById(R.id.text_input_codigo_prod);
        layout_nombre = findViewById(R.id.text_input_nombre_prod);
        layout_unidad = findViewById(R.id.text_input_unidad_prod);
        layout_categoria = findViewById(R.id.text_input_categoria_prod);
        layout_cantidad = findViewById(R.id.text_input_cantidad_prod);
        layout_cantidad_reserva = findViewById(R.id.text_input_cantidad_reserva_prod);
        layout_precio_compra = findViewById(R.id.text_input_precio_compra_prod);
        layout_precio_venta = findViewById(R.id.text_input_precio_venta_prod);
        layout_fecha_venc = findViewById(R.id.text_input_fecha_venc_prod);
        layout_info = findViewById(R.id.text_input_info_prod);

        codigo = findViewById(R.id.editText_codigo_prod);
        nombre = findViewById(R.id.editText_nombre_prod);
        unidad = findViewById(R.id.editText_unidad_prod);
        categoria = findViewById(R.id.editText_categoria_prod);
        cantidad = findViewById(R.id.editText_cantidad_prod);
        cantidad_reserva = findViewById(R.id.editText_cantidad_reserva_prod);
        precio_compra = findViewById(R.id.editText_precio_compra_prod);
        precio_venta = findViewById(R.id.editText_precio_venta_prod);
        fecha_venc = findViewById(R.id.editText_fecha_venc_prod);
        info = findViewById(R.id.editText_info_prod);

        hideKeyboard();

        if (opcion.equals("registrar")){
            toolbar.setTitle("Agregar Producto");
            codigo.setText(String.valueOf(controller.getNewCodigo()));

            habilitarCampos();

        } else if (opcion.equals("ver")){
            toolbar.setTitle("Detalle del Producto");
            productoID = getIntent().getExtras().getInt("productoID");
            cargarProducto();

            deshabilitarCampos();
        }
    }

    @Override public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_form_editar1, this.menu);

        item_edit = menu.getItem(0);
        item_add_delete = menu.getItem(1);

        if (opcion.equals("registrar")){
            item_edit.setVisible(false);
            item_edit.setEnabled(false);
            item_add_delete.setIcon(getResources().getDrawable(R.drawable.ic_check_white));

        } else if (opcion.equals("ver")){
            item_edit.setVisible(true);
            item_edit.setEnabled(true);
            item_add_delete.setIcon(getResources().getDrawable(R.drawable.ic_delete_white));
        }

        return super.onCreateOptionsMenu(this.menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_or_delete_1:
                if (opcion.equals("registrar")){
                    if (validar()) {
                        // Se guarda en la base de datos
                        Producto producto = new Producto();
                        producto.setCodigo(Integer.valueOf(codigo.getText().toString()));
                        producto.setNombre(nombre.getText().toString());
                        producto.setUnidad_cod(idUnidad);
                        producto.setCategoria_cod(idCategoria);
                        producto.setCantidad(Integer.valueOf(cantidad.getText().toString()));
                        producto.setCantidad_reserv(Integer.valueOf(cantidad_reserva.getText().toString()));
                        producto.setPrecio_compra(Double.valueOf(precio_compra.getText().toString()));
                        producto.setPrecio_venta(Double.valueOf(precio_venta.getText().toString()));
                        producto.setFecha(fecha_venc.getText().toString());
                        producto.setInfo(info.getText().toString());

                        producto.setImage(null);

                        long id = controller.addProducto(producto);

                        if (id > 0)
                            Toast.makeText(this, "Producto registrado exitosamente.", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(this, "Error al registrar.", Toast.LENGTH_SHORT).show();

                        vaciar();
                    }
                } else if (opcion.equals("ver")){
                    // borrar registro

                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
                    String mensaje = "¿Desea eliminar ese producto?";
                    dlgAlert.setMessage(mensaje);
                    dlgAlert.setTitle("Eliminar");
                    dlgAlert.setPositiveButton("Eliminar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    eliminarProducto();
                                    onBackPressed();
                                }
                            });
                    dlgAlert.setNegativeButton("Cancelar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    dlgAlert.create().show();

                } else if (opcion.equals("editar")){
                    Toast.makeText(this, "Debe guardar primero las modificaciones realizadas.", Toast.LENGTH_SHORT).show();
                }

                return true;

            case R.id.action_edit_1:
                if (opcion.equals("ver")){
                    opcion = "editar";
                    habilitarCampos();
                    toolbar.setTitle("Editar Producto");
                    item_edit.setIcon(getResources().getDrawable(R.drawable.ic_check_white));
                } else if (opcion.equals("editar")){
                    opcion = "ver";
                    deshabilitarCampos();
                    item_edit.setIcon(getResources().getDrawable(R.drawable.ic_edit_white));

                    if (validar()) {
                        // Se edita en la base de datos
                        Producto producto = new Producto();
                        producto.setCodigo(Integer.valueOf(codigo.getText().toString()));
                        producto.setNombre(nombre.getText().toString());
                        producto.setUnidad_cod(idUnidad);
                        producto.setCategoria_cod(idCategoria);
                        producto.setCantidad(Integer.valueOf(cantidad.getText().toString()));
                        producto.setCantidad_reserv(Integer.valueOf(cantidad_reserva.getText().toString()));
                        producto.setPrecio_compra(Double.valueOf(precio_compra.getText().toString()));
                        producto.setPrecio_venta(Double.valueOf(precio_venta.getText().toString()));
                        producto.setFecha(fecha_venc.getText().toString());
                        producto.setInfo(info.getText().toString());

                        producto.setImage(null);

                        long rows = controller.editProducto(producto);

                        if (rows > 0)
                            Toast.makeText(this, "Producto modificado exitosamente.", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(this, "Error al modificar.", Toast.LENGTH_SHORT).show();
                    }
                }

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because January is zero
                final String selectedDate = day + " / " + (month+1) + " / " + year;
                fecha_venc.setText(selectedDate);
            }
        });

        newFragment.show(this.getSupportFragmentManager(), "datePicker");
    }

    private void showListaUnidadDialog(){
        ListaUnidadDialog form = new ListaUnidadDialog();
        form.show(getSupportFragmentManager(), "ListaUnidadDialog");
    }

    private void showListaCategoriaProdDialog(){
        ListaCategoriaProdDialog form = new ListaCategoriaProdDialog();
        form.show(getSupportFragmentManager(), "ListaCategoriaProdDialog");
    }

    private void hideKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void eliminarProducto(){
        long rowDelete = controller.deleteProducto(productoID);
        if (rowDelete > 0)
            Toast.makeText(this, "Producto eliminado.", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Error al eliminar.", Toast.LENGTH_SHORT).show();
    }

    private void habilitarCampos(){
        codigo.setEnabled(false);
        nombre.setEnabled(true);
        unidad.setEnabled(true);
        categoria.setEnabled(true);
        cantidad.setEnabled(true);
        cantidad_reserva.setEnabled(true);
        precio_compra.setEnabled(true);
        precio_venta.setEnabled(true);
        fecha_venc.setEnabled(true);
        info.setEnabled(true);

        fecha_venc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        unidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showListaUnidadDialog();
            }
        });

        categoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showListaCategoriaProdDialog();
            }
        });

        nombre.requestFocus();
    }

    private void deshabilitarCampos(){
        codigo.setEnabled(false);
        nombre.setEnabled(false);
        unidad.setEnabled(false);
        categoria.setEnabled(false);
        cantidad.setEnabled(false);
        cantidad_reserva.setEnabled(false);
        precio_compra.setEnabled(false);
        precio_venta.setEnabled(false);
        fecha_venc.setEnabled(false);
        info.setEnabled(false);

        codigo.requestFocus();
    }

    // Validacion de campos de resgistro
    private boolean validar() {
        boolean valido = true;

        if (TextUtils.isEmpty(nombre.getText())) {
            layout_nombre.setError("Debe ingresar Nombre de producto.");
            layout_nombre.setErrorEnabled(true);
            valido = false;
        }else{
            layout_nombre.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(unidad.getText())) {
            layout_unidad.setError("Debe ingresar Unidad.");
            layout_unidad.setErrorEnabled(true);
            valido = false;
        }else{
            layout_unidad.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(categoria.getText())) {
            layout_categoria.setError("Debe ingresar Categoria.");
            layout_categoria.setErrorEnabled(true);
            valido = false;
        }else{
            layout_categoria.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(cantidad.getText())) {
            layout_cantidad.setError("Debe ingresar Cantidad de producto.");
            layout_cantidad.setErrorEnabled(true);
            valido = false;
        }else {
            layout_cantidad.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(cantidad_reserva.getText())) {
            layout_cantidad_reserva.setError("Debe ingresar Cantidad de Reserva de producto.");
            layout_cantidad_reserva.setErrorEnabled(true);
            valido = false;
        }else {
            layout_cantidad_reserva.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(precio_compra.getText())) {
            layout_precio_compra.setError("Debe ingresar Precio de compra.");
            layout_precio_compra.setErrorEnabled(true);
            valido = false;
        }else {
            layout_precio_compra.setErrorEnabled(false);
        }

        if (TextUtils.isEmpty(precio_venta.getText())) {
            layout_precio_venta.setError("Debe ingresar Precio de venta.");
            layout_precio_venta.setErrorEnabled(true);
            valido = false;
        }else {
            layout_precio_venta.setErrorEnabled(false);
        }

        return valido;
    }

    private void vaciar(){
        codigo.setText(String.valueOf(controller.getNewCodigo()));
        nombre.setText("");
        unidad.setText("");
        categoria.setText("");
        cantidad.setText("");
        cantidad_reserva.setText("");
        precio_compra.setText("");
        precio_venta.setText("");
        fecha_venc.setText("");
        info.setText("");

        nombre.requestFocus();
    }

    private void cargarProducto(){
        Producto producto = controller.getProductoByID(productoID);
        codigo.setText(String.valueOf(producto.getCodigo()));
        nombre.setText(producto.getNombre());
        cargarUnidad(producto.getUnidad_cod());
        cargarCategoria(producto.getCategoria_cod());
        cantidad.setText(String.valueOf(producto.getCantidad()));
        cantidad_reserva.setText(String.valueOf(producto.getCantidad_reserv()));
        precio_compra.setText(String.valueOf(producto.getPrecio_compra()));
        precio_venta.setText(String.valueOf(producto.getPrecio_venta()));
        fecha_venc.setText(producto.getFecha());
        info.setText(producto.getInfo());
    }

    private void cargarUnidad(int id){
        UnidadController controller = new UnidadController(this);
        Unidad unid = controller.getUnidadByID(id);
        unidad.setText(unid.getNombre());
        idUnidad = id;
    }

    private void cargarCategoria(int id){
        CategoriaProdController controller = new CategoriaProdController(this);
        CategoriaProd cate = controller.getCategoriaByID(id);
        categoria.setText(cate.getNombre());
        idCategoria = id;
    }

    // Listeners de ElementoDialog
    @Override
    public void onCloseDialog(String message) {
        JSONObject jsonObject;
        String tipoDialog = "";
        String valor = "";
        int id = 0;

        try {
            jsonObject = new JSONObject(message);
            tipoDialog = jsonObject.getString("tipoDialog");
            valor = jsonObject.getString("valor");
            id = jsonObject.getInt("valorID");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (tipoDialog.equals("unidad")){
            unidad.setText(valor);
            idUnidad = id;
        }
        else if (tipoDialog.equals("categoria_prod")){
            categoria.setText(valor);
            idCategoria = id;
        }
    }

    @Override
    public void onCancelDialog() {

    }

    @Override
    public void onReturnUnidadValue(int id, String nombre) {
        unidad.setText(nombre);
        idUnidad = id;
    }

    @Override
    public void onReturnCategoriaProdValue(int id, String nombre) {
        categoria.setText(nombre);
        idCategoria = id;
    }
}
